<?php

namespace Fattura24;
use Automattic\WooCommerce\Blocks\Integrations\IntegrationInterface;

define('Fattura24BillingBlock_VERSION', '0.2.0');

defined('ABSPATH') || die;

/**
 * Class for integrating with WooCommerce Blocks
 */
class Fattura24BillingBlock_Blocks_Integration implements IntegrationInterface {

    /**
     * The name of the integration.
     *
     * @return string
     */
    public function get_name() {
        return 'fattura24-billing-block';
    }

    /**
     * When called invokes any initialization/setup for the integration.
     */
    public function initialize() {
        $this->register_billing_block_editor_scripts();
        $this->register_billing_block_frontend_scripts();
        $this->register_billing_block_editor_styles();
        $this->register_main_integration();
    }

    /**
     * Registers the main JS file required to add filters and Slot/Fills.
     */
    public function register_main_integration() {
        $script_path = '/build/index.js';
        $style_path  = '/build/style-index.css';

        $script_url = plugins_url($script_path, __FILE__);
        $style_url  = plugins_url($style_path, __FILE__);

        $script_asset_path = dirname(__FILE__) . '/build/index.asset.php';
        $script_asset      = file_exists($script_asset_path)
            ? require $script_asset_path
            : array(
                'dependencies' => array(),
                'version'      => $this->get_file_version($script_path),
            );

        wp_enqueue_style(
            'fattura24-billing-block-blocks-integration',
            $style_url,
            [],
            $this->get_file_version($style_path)
        );

        wp_register_script(
            'fattura24-billing-block-blocks-integration',
            $script_url,
            $script_asset['dependencies'],
            $script_asset['version'],
            true
        );
        wp_set_script_translations(
            'fattura24-billing-block-blocks-integration',
            'fattura24',
            dirname(__FILE__, 2) . '/languages'
        );
    }

    public function register_billing_block_editor_scripts() {
        $script_path       = '/build/fattura24-billing-block.js';
        $script_url        = plugins_url($script_path, __FILE__);
        $script_asset_path = dirname(__FILE__) . '/build/fattura24-billing-block.asset.php';
        $script_asset      = file_exists($script_asset_path)
            ? require $script_asset_path
            : array(
                'dependencies' => array(),
                'version'      => $this->get_file_version($script_path),
            );

        wp_register_script(
            'fattura24-billing-block-editor',
            $script_url,
            $script_asset['dependencies'],
            $script_asset['version'],
            true
        );

        wp_set_script_translations(
            'fattura24-billing-block-editor', // script handle
            'fattura24', // text domain
            dirname(__FILE__, 2) . '/languages'
        );
    }

    public function register_billing_block_frontend_scripts() {
        $script_path       = '/build/fattura24-billing-block-frontend.js';
        $script_url        = plugins_url($script_path, __FILE__);
        $script_asset_path = dirname(__FILE__) . '/build/fattura24-billing-block-frontend.asset.php';
        $script_asset      = file_exists($script_asset_path)
            ? require $script_asset_path
            : array(
                'dependencies' => array(),
                'version'      => $this->get_file_version($script_path),
            );

        wp_register_script(
            'fattura24-billing-block-frontend',
            $script_url,
            $script_asset['dependencies'],
            $script_asset['version'],
            true
        );
        wp_set_script_translations(
            'fattura24-billing-block-frontend', // script handle
            'fattura24', // text domain
            dirname(__FILE__, 2) . '/languages'
        );
    }

    /**
     * Returns an array of script handles to enqueue in the frontend context.
     *
     * @return string[]
     */
    public function get_script_handles() {
        return array('fattura24-billing-block-blocks-integration', 'fattura24-billing-block-frontend');
    }

    /**
     * Returns an array of script handles to enqueue in the editor context.
     *
     * @return string[]
     */
    public function get_editor_script_handles() {
        return array('fattura24-billing-block-blocks-integration', 'fattura24-billing-block-editor');
    }

    /**
     * An array of key, value pairs of data made available to the block on the client side.
     *
     * @return array
     */
    public function get_script_data() {
        
        /** includo i file dove si trovano le funzioni richiamate alle righe 162 e 163 */
        $filesToInclude = [
            'met_hooks_fields.php',
            'met_settings_sections.php'
        ];
        
        foreach ($filesToInclude as $file) {
            require_once plugin_dir_path(__FILE__) . '../src/methods/' . $file;
        }

        $showBillingCheckbox = fatt_24_show_billing_checkbox();    
        $fattura24Settings = fatt_24_get_settings();
        $user_id = get_current_user_id();

        $f24BillingFields = array(
            'billing_fiscalcode' => '',
            'billing_vatcode' => ''
        );

        if ($user_id) {
            foreach($f24BillingFields as $key => $value) {
                $f24BillingFields[$key] = get_user_meta($user_id, $key, true);
            }
        }
        
        $data = array(
            'fattura24-billing-block-active' => true,
            'showBillingCheckbox' => $showBillingCheckbox,
            'fattura24Settings' => $fattura24Settings,
            'invoiceTypes' => array(
                FATT_24_DT_FATTURA, 
                FATT_24_DT_RICEVUTA, 
                FATT_24_DT_FATTURA_FORCED, 
                FATT_24_DT_FATTURA_ELETTRONICA
            ),
            'f24BillingFields' => $f24BillingFields
        );

        return $data;

    }

    public function register_billing_block_editor_styles() {
        $style_path  = '/build/style-fattura24-billing-block.css';

        $style_url  = plugins_url($style_path, __FILE__);
        wp_enqueue_style(
            'fattura24-billing-block-fattura24-checkout-block',
            $style_url,
            [],
            $this->get_file_version($style_path)
        );
    }

    /**
     * Get the file modified time as a cache buster if we're in dev mode.
     *
     * @param string $file Local path to the file.
     * @return string The cache buster value to use for the given file.
     */
    protected function get_file_version($file) {
        if (defined('SCRIPT_DEBUG') && SCRIPT_DEBUG) {
            try { 
                if (file_exists($file)) {
                    return filemtime($file);
                }

                $local_path = dirname(__FILE__) . $file;
                if (file_exists($local_path)) {
                    return filemtime($local_path);
                }
            } catch (\Exception $e) {
                // per evitare problemi lo faccio finire nel debug di WP
                error_log('errore fatt_24_billing_blocks: ', print_r($e, true));
            }
        }
        return Fattura24BillingBlock_VERSION;
    }
}
