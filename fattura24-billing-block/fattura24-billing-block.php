<?php

namespace Fattura24;

defined('ABSPATH') || die;

require_once __DIR__ . '/fattura24-billing-block-blocks-integration.php';

add_action(
   'woocommerce_blocks_cart_block_registration',
	function( $integration_registry ) {
		$integration_registry->register( new Fattura24BillingBlock_Blocks_Integration() );
	}
);

add_action(
	'woocommerce_blocks_checkout_block_registration',
	function( $integration_registry ) {
		$integration_registry->register( new Fattura24BillingBlock_Blocks_Integration() );
	}
);


/**
 * Registers the slug as a block category with WordPress.
 */
function register_Fattura24BillingBlock_block_category( $categories ) {
    return array_merge(
        $categories,
        [
            [
                'slug'  => 'fattura24-billing-block',
                'title' => __( 'Fattura24BillingBlock Blocks', 'fattura24' ),
            ],
        ]
    );
}
add_action( 'block_categories_all', __NAMESPACE__ .'\register_Fattura24BillingBlock_block_category', 10, 2 );
