/**
 * External dependencies
 */
import { useEffect, useState } from '@wordpress/element';
import { CheckboxControl, TextInput, ValidationInputError } from '@woocommerce/blocks-checkout';
import { useDispatch } from '@wordpress/data';
import { __, sprintf } from '@wordpress/i18n';

// Constants
const SETTINGS = window.wcSettings['fattura24-billing-block_data'];
const {
    showBillingCheckbox,
    fattura24Settings,
    f24BillingFields,
} = SETTINGS;

const {
    'fatt-24-inv-create': invOption,
    'fatt-24-abk-fiscode-req': cfRequired,
    'fatt-24-abk-vatcode-req': pivaRequired,
    'fatt-24-inv-disable-receipts': alwaysCreateInvoice,
    'fatt-24-add-vat-field': vatAddedBy,
} = fattura24Settings;

const EU_COUNTRIES = [
    'AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL',
    'ES', 'FI', 'FR', 'GR', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV',
    'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK'
];

const ERROR_MESSAGES = {
    /* translators: %1$s and %2$s are placeholders for 'VAT number' and 'Fiscal code' that change position according to context */
    both: '%1s required in lack of %2s',

    vat: {
        default: __('VAT Number required', 'fattura24'),
        length: __('VAT Number should be long 11 chars', 'fattura24')
    },
    cf: {
        default: __('Fiscal code required', 'fattura24'),
        length: __('Fiscal code should be long 11 or 16 chars', 'fattura24')
    },
    sdi: __('SDI code should be long 6 or 7 chars', 'fattura24'),
    pec: __('PEC address is not a valid email', 'fattura24')
};

const EMAIL_REGEX = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

// Default form data
const DEFAULT_DATA = {
    billing_checkbox: f24BillingFields?.billing_vatcode ? true : false,
    billing_fiscalcode: f24BillingFields?.billing_fiscalcode || '',
    billing_vatcode: f24BillingFields?.billing_vatcode || '',
    billing_recipientcode: '',
    billing_pecaddress: ''
};

// Helper functions
const isEU = country => EU_COUNTRIES.includes(country);
const isVatNumberRequired = vatAddedBy === 'WooCommerce Fattura24';

const getResultingDocType = (country, billing_vatcode, customerRequiredInvoice) => {
    const invOptionNum = parseInt(invOption);
    
    switch (invOptionNum) {
        case 0:
            return 'R';
        case 1:
            return (alwaysCreateInvoice || customerRequiredInvoice) ? 'I-force' : 
                   (!billing_vatcode ? 'I' : 'R');
        case 2:
            return alwaysCreateInvoice ? 'FE' : 
                   (country === 'IT' && !billing_vatcode ? 'R' : 'R');
        case 3:
        case 4:
            return customerRequiredInvoice ? 'FE' : 
                   (customerRequiredInvoice && country === 'IT' ? 'FE' : 'R');
        case 6:
            return customerRequiredInvoice ? 'FE' : 
                   (isEU(country) && customerRequiredInvoice ? 'FE' : 'R');
        case 7:
            return customerRequiredInvoice ? 'FE' : 
                   (isEU(country) && customerRequiredInvoice ? 'FE' : 'I-force');
        default:
            return 'R';
    }
};

const Block = ({ checkoutExtensionData, cart }) => {
    // Form state
    const [formData, setFormData] = useState(DEFAULT_DATA);

    // Error state
    const [validationErrors, setValidationErrors] = useState({});
    const [fieldErrors, setFieldErrors] = useState({
        cf: false,
        piva: false,
        sdi: false,
        pec: false
    });

    const { setExtensionData } = checkoutExtensionData;
    const { VALIDATION_STORE_KEY } = window.wc.wcBlocksData;
    const dispatch = useDispatch(VALIDATION_STORE_KEY);
    const [country, setCountry] = useState(cart.billingAddress.country);

    // Update country when billing address changes
    useEffect(() => {
        if (cart.billingAddress?.country) {
            setCountry(cart.billingAddress.country);
        }
    }, [cart.billingAddress]);

    // Error handling
    const setError = (field, message) => {
        const errorKey = `fattura24-billing-block/${field}`;
        setValidationErrors(prev => ({
            ...prev,
            [errorKey]: { message }
        }));
        dispatch.setValidationErrors?.({
            [errorKey]: { message }
        });
    };

    const clearError = (field) => {
        const errorKey = `fattura24-billing-block/${field}`;
        setValidationErrors(prev => {
            const newErrors = { ...prev };
            delete newErrors[errorKey];
            return newErrors;
        });
        dispatch.clearValidationError?.(errorKey);
    };

    // Form handlers
    const handleInputChange = (field, value) => {
        setFormData(prev => ({ ...prev, [field]: value }));
        setExtensionData('fattura24-billing-block', field, value);
    };

    const handleCheckboxChange = (isChecked) => {
        handleInputChange('billing_checkbox', isChecked);
        if (!isChecked) {
            handleInputChange('billing_vatcode', '');
        }
    };

    // Validation
    useEffect(() => {
        const { billing_checkbox, billing_vatcode, billing_fiscalcode, 
                billing_recipientcode, billing_pecaddress } = formData;

        const resultingDocType = getResultingDocType(country, billing_vatcode, billing_checkbox);
        const isItalian = country === 'IT';

        // VAT validation
        if (isVatNumberRequired && 
            (pivaRequired || billing_checkbox || resultingDocType === 'FE') && 
            ((isItalian && !billing_fiscalcode && billing_vatcode.length !== 11) || 
             (!isItalian && !billing_vatcode))) {
            setFieldErrors(prev => ({ ...prev, piva: true }));
            setError('billing_vatcode', 
                (!billing_vatcode && !billing_fiscalcode) && isItalian? sprintf(__(ERROR_MESSAGES.both,'fattura24'),__('VAT Number', 'fattura24'), __('Fiscal code', 'fattura24')) :
                billing_vatcode && isItalian ? ERROR_MESSAGES.vat.length :
                ERROR_MESSAGES.vat.default
            );
        } else {
            setFieldErrors(prev => ({ ...prev, piva: false }));
            clearError('billing_vatcode');
        }

        // Additional validations for Italian customers
        if (isItalian) {
            // Fiscal code validation
            const validFiscalCodeLength = [11, 16].includes(billing_fiscalcode.length);
            if (isVatNumberRequired && !validFiscalCodeLength && !billing_vatcode && 
                (cfRequired || billing_checkbox || resultingDocType === 'FE')) {
                setFieldErrors(prev => ({ ...prev, cf: true }));
                setError('billing_fiscalcode',
                    (!billing_fiscalcode && !billing_vatcode) && isItalian ? sprintf(__(ERROR_MESSAGES.both,'fattura24'),__('Fiscal code', 'fattura24'), __('VAT Number', 'fattura24')):
                    billing_fiscalcode ? ERROR_MESSAGES.cf.length :
                    ERROR_MESSAGES.cf.default
                );
            } else {
                setFieldErrors(prev => ({ ...prev, cf: false }));
                clearError('billing_fiscalcode');
            }

            // SDI code validation
            const validSDILength = [6, 7].includes(billing_recipientcode.length);
            if (billing_recipientcode && !validSDILength) {
                setFieldErrors(prev => ({ ...prev, sdi: true }));
                setError('billing_recipientcode', ERROR_MESSAGES.sdi);
            } else {
                setFieldErrors(prev => ({ ...prev, sdi: false }));
                clearError('billing_recipientcode');
            }

            // PEC address validation
            if (billing_pecaddress && !EMAIL_REGEX.test(billing_pecaddress)) {
                setFieldErrors(prev => ({ ...prev, pec: true }));
                setError('billing_pecaddress', ERROR_MESSAGES.pec);
            } else {
                setFieldErrors(prev => ({ ...prev, pec: false }));
                clearError('billing_pecaddress');
            }
        }
    }, [formData, country]);

    const showTitle = () => showBillingCheckbox || isVatNumberRequired || country === 'IT';

    return (
        <>
            {showTitle() && (
                <h2 className="wc-block-components-title wc-block-components-checkout-step__title">
                    {__('Other invoicing data', 'fattura24')}
                </h2>
            )}
            
            {showBillingCheckbox && (
                <CheckboxControl
                    id="billing_checkbox"
                    checked={formData.billing_checkbox}
                    onChange={handleCheckboxChange}
                >
                    {__('I would like to receive an invoice', 'fattura24')}
                </CheckboxControl>
            )}

            {isVatNumberRequired && (
                <>
                    <TextInput
                        className={fieldErrors.piva ? 'has-error' : ''}
                        label={__('VAT Number', 'fattura24')}
                        value={formData.billing_vatcode}
                        onChange={(value) => handleInputChange('billing_vatcode', value)}
                    />
                    {fieldErrors.piva && validationErrors['fattura24-billing-block/billing_vatcode'] && (
                        <ValidationInputError
                            errorMessage={validationErrors['fattura24-billing-block/billing_vatcode'].message}
                        />
                    )}
                </>
            )}

            {country === 'IT' && (
                <>
                    <TextInput
                        className={fieldErrors.cf ? 'has-error' : ''}
                        label={__('Fiscal Code', 'fattura24')}
                        value={formData.billing_fiscalcode}
                        onChange={(value) => handleInputChange('billing_fiscalcode', value)}
                    />
                    {fieldErrors.cf && validationErrors['fattura24-billing-block/billing_fiscalcode'] && (
                        <ValidationInputError
                            errorMessage={validationErrors['fattura24-billing-block/billing_fiscalcode'].message}
                        />
                    )}

                    <TextInput
                        className={fieldErrors.pec ? 'has-error' : ''}
                        label={__('PEC Address (optional)', 'fattura24')}
                        value={formData.billing_pecaddress}
                        onChange={(value) => handleInputChange('billing_pecaddress', value)}
                    />
                    {fieldErrors.pec && validationErrors['fattura24-billing-block/billing_pecaddress'] && (
                        <ValidationInputError
                            errorMessage={validationErrors['fattura24-billing-block/billing_pecaddress'].message}
                        />
                    )}

                    <TextInput
                        className={fieldErrors.sdi ? 'has-error' : ''}
                        label={__('SDI Code (optional)', 'fattura24')}
                        value={formData.billing_recipientcode}
                        onChange={(value) => handleInputChange('billing_recipientcode', value)}
                    />
                    {fieldErrors.sdi && validationErrors['fattura24-billing-block/billing_recipientcode'] && (
                        <ValidationInputError
                            errorMessage={validationErrors['fattura24-billing-block/billing_recipientcode'].message}
                        />
                    )}
                </>
            )}
        </>
    );
};

export default Block;