<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Descrizione: schermata Impostazioni->Fattura24->Chiamate API - (parte visiva)
 * le funzioni di servizio sono in methods/met_api_info.php
 */

namespace Fattura24;

defined('ABSPATH') || die;

$filesToInclude = [ 
    'methods/met_api_info.php'
];

foreach ($filesToInclude as $file) {
    require_once FATT_24_CODE_ROOT . $file;
}

function fatt_24_show_api_info() {
    $api_calls_table = new Fattura24_API_Calls_Table();
    $api_calls_table->prepare_items();
    $total_record = $api_calls_table->total_items;
    $over_500_calls_msg = __('You overcame API calls daily limit!', 'fattura24');
    $total_calls_msg = __('Last 24 hours total calls : ', 'fattura24');
    $text_color = $total_record >= 500 ? 'red' : ($total_record > 450 ? 'orange' : 'green');
    $total_record_color = '<span style="color:' . $text_color . '">' . $total_record . '</span>';

    ?>
    <div class='wrap'>
        <h2></h2>
        <?php 
        fatt_24_get_link_and_logo(__('', 'fatt-24-api-info')); 
        echo fatt_24_build_nav_bar();
        ?>
        <div>
            <table>
                <tr>
                    <td style="vertical-align: top;">
                        <div style="padding:10px; font-size:150%;"><strong><?php _e('F24 API call logs stats', 'fattura24') ?></strong></div>
                        <div style="padding:10px; font-size:120%;">
                            <?php 
                            echo $total_calls_msg . $total_record_color;
                            if ($total_record >= 500) {
                                echo '<span style="color:red;"> ' . $over_500_calls_msg . '</span>';
                            }
                            ?>
                        </div>
                        <div style="padding:10px; font-size:120%;">
                            <?php _e('Here below you find fattura24 API call logs for your key. <br />
                            In "Command" column you find the command you sent, in "Source" you find the starting point of the call. <br /> 
                            For instance: F24-Woo 4.8.5 means that you used fattura24 WooCommerce plugin version 4.8.5 .', 'fattura24');
                            ?>
                        </div>
                        <br />
                        <form method="post">
                            <?php $api_calls_table->display(); ?>
                        </form>
                    </td>
                    <td style='width:250px; vertical-align: top;'><?php echo fatt_24_infobox(); ?></td>
                </tr>
            </table>
        </div>
    </div>
    <?php
}