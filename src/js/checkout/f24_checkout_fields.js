/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Script per gestire la logica mostra/nascondi
 * dei campi fiscali nella pagina di pagamento
 * (visualizzazione classica o shortcodes)
 * 
 */

jQuery(document).ready(function($) {
    
    const additionalVars = {
        alwaysCreateFE: f24_checkout_vars['alwaysCreateFE'] === '1',
        cfRequired: f24_checkout_vars['cfRequired'] === '1',
        pIRequired: f24_checkout_vars['pIRequired'] === '1',
        showCheckbox: f24_checkout_vars['showCheckbox'] === '1',
        vat_message: f24_checkout_vars['vat_message'],
        cf_message: f24_checkout_vars['cf_message']
    };

    // All additional Fattura24 fields + 'billing_company' (WooCommerce native)
    const allFields = [
        '#billing_company', 
        '#billing_fiscalcode', 
        '#billing_vatcode', 
        '#billing_pecaddress', 
        '#billing_recipientcode'
    ];

    // Helper functions to filter fields;
    const isOtherField = field => ['#billing_company', '#billing_vatcode'].includes(field);
    const isInvoiceField = field => ['#billing_fiscalcode', '#billing_vatcode'].includes(field);

    const {
        alwaysCreateFE,
        cfRequired,
        pIRequired,
        showCheckbox,
    } = additionalVars;

    // required symbol
    const required = '<abbr class="required" title="required">*</abbr>';
    function getOneRequired(field) {
        const title = field === '#billing_vatcode' ? 
            additionalVars.vat_message : 
            additionalVars.cf_message;
        
        return `<abbr class="required" title="${title}">°</abbr>`;
    }
    //const oneRequired = '<abbr class="required" title="' + title + '">°</abbr>';
    let appendedSymbol = false;

    // show/hide field function
    function showHideField(field, visible) {
        const element = $(field);
        const elementField = $(field + '_field');
        const label = $(field + '_field label');

        if (visible) {
            element.show();
            elementField.show();
            label.show();
        } else {
            element.hide();
            elementField.hide();
            elementField.removeClass('validate-required woocommerce-validated woocommerce-invalid woocommerce-invalid-required-field');
            label.hide();
        }
    }

    /**
     * Add (°) symbol only for cf and vat code:
     * since customer asked for invoice filling in one field
     * between fiscal code and vat code is required
     */
    function appendOneRequired() {
        if (appendedSymbol) {
            return;
        }

        allFields.forEach(field => {
            if (isInvoiceField(field)) {
                $(field + '_field label > .optional').remove();
                $(field + '_field label').append(getOneRequired(field));
            }
        });
        appendedSymbol = true;
    }

    /**
    * Add required symbol (*) only for cf and vat code
    */
    function fieldRequired() {
        if (appendedSymbol) {
            return;
        }

        const isIT = $('#billing_country').val() === 'IT';
        allFields.forEach(field => {
            if ((cfRequired && field === '#billing_fiscalcode' && isIT) || (pIRequired && field === '#billing_vatcode')) {
                $(field + '_field label > .optional').remove();
                $(field + '_field label').append(required);
            }
        });
        appendedSymbol = true;
    }

    /** Additional check on Cf required  */
    function setCfRequired() {
        let fieldCF = '#billing_fiscalcode';
        let isOptional = $(fieldCF + '_field label > .optional').length > 0;

        if (cfRequired && isOptional) {
            $(fieldCF + '_field label > .optional').remove();
            $(fieldCF + '_field label').append(required);
        }
    }

    /**
     * Main function: switch hide/show logic of fields and handles required attribute
     * uses showHideField(field, visible)
     */
    function toggleFields() {
        const isIT = $('#billing_country').val() === 'IT';
        const checkbox = $('#billing_checkbox');
        const vatCodeField = $('#billing_vatcode');
        const fiscalCodeField = $('#billing_fiscalcode');
        const isChecked = $('#billing_checkbox').is(':checked');

        if (vatCodeField.val()) {
            checkbox.prop('checked', true);
        }

        if (cfRequired || pIRequired) {
            fieldRequired();
        } else if (alwaysCreateFE || isChecked) {
            appendOneRequired();
        }

        // helper to check which field has to be shown
        const shouldShowField = field => {
            if (cfRequired && isIT && field === '#billing_fiscalcode') return true;
            if (pIRequired && field === '#billing_vatcode') return true;
            if (!showCheckbox) return isIT || isOtherField(field);
            if (isIT) return isChecked;
            return isChecked && isOtherField(field);
        }

        allFields.forEach(field => {
            showHideField(field, shouldShowField(field));
        });

        // show fiscal code if billing_country === IT and not empty
        if (fiscalCodeField.val() && isIT) {
            showHideField('#billing_fiscalcode', true);
        }
        
        // show vat code if not empty
        if (vatCodeField.val()) {
            showHideField('#billing_vatcode', true);
        }

        setCfRequired();
    }

    $('#billing_checkbox').change(function() {
        if (cfRequired || pIRequired) {
            fieldRequired();
        } else {
            appendOneRequired();
        }

        if (!$(this).is(':checked')) {
            $('#billing_vatcode').val('');
        }
        toggleFields();
    });

    $('#billing_country').change(toggleFields);

    toggleFields();
});
