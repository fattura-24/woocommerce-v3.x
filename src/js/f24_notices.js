/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Script legato ai messaggi ignorabili lato amministrazione.
 * I messaggi ignorabili sono quelli con la "x" in alto a destra
 * la chiamata AJAX wp_ajax_fatt_24_dismiss_notice (in src/messages.php)
 * fa sì che l'azione venga registrata in WordPress: in questo modo il messaggio
 * non ricompare ricaricando la pagina o cambiando la pagina lato amministrazione
 */

(function($) {
    window.addEventListener('load', () => {
        $('.notice.is-dismissible').on('click', '.notice-dismiss', function() {
            let $notice = $(this).closest('.notice');
            let messageId = $notice.attr('id');

            $.post(ajaxurl, {
                action: 'fatt_24_dismiss_notice',
                message_id: messageId
            });

            $notice.fadeOut();
        });
    })
})(jQuery);    