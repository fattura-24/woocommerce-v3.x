/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Script per caricare i dati fiscali del cliente
 * insieme agli altri dati anagrafici
 * utilizzato in src/classes/fattura24-extend-woo-core.php
 * dal metodo load_f24_custom_billing_data_admin
 * 
 */

jQuery(document).ready(function($) {
    function populateFattura24Fields(userId) {
        var fields = [
            'billing_fiscalcode',
            'billing_vatcode',
            'billing_pecaddress',
            'billing_recipientcode'
        ];

        // Fetch customer data using AJAX
        $.ajax({
            url: ajaxurl,
            data: {
                user_id: userId,
                action: 'woocommerce_get_customer_details',
                security: woocommerce_admin_meta_boxes.get_customer_details_nonce 

            },
            type: 'POST',
            dataType: 'json',
            success: function(response) {
                // console.log("Customer data received:", response);
                
                fields.forEach(function(field) {
                    let $field = $('#_' + field);

                    if (!$field.length) {
                        console.error('Field not found :', field);
                        return;
                    }

                    let value = response.billing[field] || '';
                    if (['billing_fiscalcode', 'billing_recipientcode'].includes(field)) {
                        value = value.toUpperCase();
                    }

                    $field.val(value).trigger('change');
                });
            },
            error: function(xhr, status, error) {
                console.error("Error fetching customer data:", error);
            }
        });
    }

    let initialUserId = $('#customer_user').val();
    if (initialUserId) {
        populateFattura24Fields(initialUserId);
    }

    // Aggancia l'evento di cambio del cliente
    $(document).on('change', '#customer_user', function() {
        let userId = $(this).val();
        if (userId) {
            populateFattura24Fields(userId);
        }
    });

       // Mantieni gli eventi esistenti
    $(document).on('wc_backbone_modal_loaded', function(e, target) {
        // console.log("Modal loaded:", target);
        if (target === 'wc-modal-add-customer') {
            setTimeout(function() {
                let userId = $('#customer_user').val();
                if (userId) {
                    populateFattura24Fields(userId);
                }
            }, 100);
        }
    });

    $(document.body).on('wc_backbone_modal_response', function(e, target) {
        if (target.indexOf('wc-modal-edit-customer') !== -1 || target.indexOf('wc-modal-add-customer') !== -1) {
            setTimeout(function() {
                let userId = $('#customer_user').val();
                if (userId) {
                    populateFattura24Fields(userId);
                }
            }, 100);
        }
    });
});