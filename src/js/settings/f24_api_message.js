/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Script con cui visualizzo i messaggi dopo la chiamata API TestKey
 * I messaggi appaiono nel campo FATT_24_API_MESSAGE delle impostazioni generali del plugin
 */


window.addEventListener('load', () => {
    //console.log('messaggi :', f24_scripts_data['messages']);
    let apiInput = document.getElementById('fatt-24-API-key'); // non scomodiamo Php
    let apiMessage = f24_scripts_data['messages']['apiText'];
    let apiErrorMsg = f24_scripts_data['messages']['apiTestMsg'];
                                    
    apiInput.addEventListener('focusout', function(e){
        let inputVal = e.target.value;
        let inputLen = inputVal.length;
                
        if (inputLen !== 32 && inputLen !== 0) {
            document.getElementById("fatt-24-api-message").innerHTML = "<span style='color: red; font-size: 120%;'>" + apiErrorMsg[1]  + "</span></br>";
        } else if (inputLen === 0) {
            document.getElementById("fatt-24-api-message").innerHTML = "<span style='color: red; font-size: 120%;'>" + apiErrorMsg[0] + "</span></br>"; 
        } else {
            document.getElementById("fatt-24-api-message").innerHTML = apiMessage;
        }
    });
}); 