/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Script legato alla sezione 'Tipologie di pagamento' delle impostazioni generali
 * del modulo. 
 * Gestione logica mostra / nascondi e aggiornamento opzione scelta per il metodo di pagamento
 */

let $ = jQuery;

window.addEventListener('load', () => {
    let selectElements = [...document.getElementsByClassName('payment-types')];
    let applyButtons = [...document.getElementsByClassName('button apply-type')];

    function toggleElements(select) {
        let payment_gateway_id = select.dataset.id;
        let button = document.querySelector(`.button.apply-type[data-id='${payment_gateway_id}']`);
        let initialValue = select.getAttribute('data-initial-value');
        let hasStoredValue = select.getAttribute('data-has-stored-value') === '1';

        if (button) {
            if (!hasStoredValue || (select.value && select.value !== initialValue)) {
                button.style.display = 'inline-block';
            } else {
                button.style.display = 'none';
            }
        }
    }    

    selectElements.forEach(select => {
        select.setAttribute('data-initial-value', select.value);
        toggleElements(select);
        select.addEventListener('change', () => toggleElements(select));
    });

    applyButtons.forEach(button => {
        button.addEventListener('click', (e) => {
            e.preventDefault();
            let gateway_id = e.target.dataset.id;
            let select = document.querySelector(`.payment-types[data-id='${gateway_id}']`);
            let type = select.value;
            if (type) {
                let data = {
                    gateway_id: gateway_id,
                    type: type
                };
                updatePaymentType(data);
            } else {
                alert('No payment type chosen');
            }
        })  
    });     

    function updatePaymentType(data) {
        let { gateway_id, type } = data;
        $.ajax({
            type: 'POST',
            url: f24_scripts_data['url'],
            data: {
                action: 'save_mp',
                nonce: f24_scripts_data['payment_types_nonce'],
                gateway_id: gateway_id,
                type: type
            },
            dataType: 'json'
        }).done(function (response) {
            if (response.success) {
                alert(response.data);
                location.reload(); // aggiorna la pagina
            } else {
                alert('Error: ' + response.data);
            }
        }).fail(function (err) {
            console.log('error :', err, 'arguments :', arguments);
            alert('Error occurred while updating. Please check the console for more details');
        })
    }
});