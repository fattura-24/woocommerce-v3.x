/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Script legato alla sezione 'Voci di ricavo' delle impostazioni generali
 * del modulo. 
 * Gestione logica mostra / nascondi e aggiornamento opzione scelta per il prodotto
 */

let $ = jQuery;

window.addEventListener('load', () => {
    let defaultSelects = [...document.getElementsByClassName('fatt-24-custom-select')];
    let selectElements = [...document.getElementsByClassName('pdc-select')];
    let messages = f24_scripts_data['messages'];

    // Event listeners for default selects
    defaultSelects.forEach(select => {
        select.setAttribute('initialValue', select.value);
        let initialValue = select.getAttribute('initialValue');
        let selectId = select.id;
        
        // uso document.querySelector perché così nascondo solo il pulsante
        let button = document.querySelector(`.button.action.fatt24.default[id='${selectId}']`);
        button.classList.add('fatt-24-hidden');
      
        select.addEventListener('change', (e) => {
            button.classList.toggle('fatt-24-hidden', !select.value || select.value === initialValue);
        });

        button.addEventListener('click', (e) => {
            if (select.value) {
                updatePdc(selectId, select.value, 'default');
                button.classList.add('fatt-24-hidden');
            } else {
                alert(messages['coa_error']);
            }
        });
    });

    // Event listeners for product-specific selects
    selectElements.forEach(select => {
        select.setAttribute('initialValue', select.value);
        let initialValue = select.getAttribute('initialValue');
        let productId = select.id;
        let button = document.querySelector(`.button.action.fatt24[data-product-id='${productId}']`);
        button.classList.toggle('fatt-24-hidden', !select.value || select.value === initialValue);
        
        select.addEventListener('change', (e) => {
            button.classList.toggle('fatt-24-hidden', !select.value || select.value === initialValue);
        });

        button.addEventListener('click', (e) => {
            if (select.value) {
                updatePdc(productId, select.value, 'table');
                button.classList.add('fatt-24-hidden');
            } else {
                alert(messages['coa_error']);
            }
        });
    });

   
    
    // Bulk update functionality
    $('#doaction, #doaction2').on('click', function(e) {
        e.preventDefault();
        
        let selectedProducts = $('input[name="product[]"]:checked').map(function() {
            return this.value;
        }).get();
       
        if (selectedProducts.length > 0) {
            let action = $('#bulk-action-selector-top').val();
            let productIds = selectedProducts.join(','); // comma separated values
            updatePdc(productIds, action, 'bulk-action');
        } else {
            alert(messages['product_error']);
        }
    });
    
    /**
     * 
     * @param {string} id => string (es.: 'products', 'shipping', 'fees') or product id or comma separated product ids 
     * @param {string} value => id revenue item in Fattura24 
     * @param {string} section => 'default', 'table', 'bulk-action'
     */
    function updatePdc(id, value, section) {
        $.ajax({
            type: 'POST',
            url: f24_scripts_data['url'],
            data: {
                action: 'save_pdc',
                nonce: f24_scripts_data['pdc_nonce'],
                ids: id,
                value: value,
                section: section
            },
            dataType: 'json'
        }).done(function(r){
            alert(messages['update_success']);
            location.reload();
        }).fail(function(err){
            console.log('error :', err, 'arguments :', arguments);
            alert(messages['update_error']);
        });
    }    
});