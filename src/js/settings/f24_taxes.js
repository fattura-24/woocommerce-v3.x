/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com> 
 *
 * Script legato alla sezione 'Natura IVA 0%' delle impostazioni generali
 * del modulo. 
 * Gestione logica mostra / nascondi e aggiornamento opzione scelta per la tassa
 */


let $ = jQuery;

window.addEventListener('load', () => {
    let selectElements = [...document.getElementsByClassName('tax-codes')];
    let applyButtons = [...document.getElementsByClassName('button apply-natura')];
    let checkboxes = [...document.getElementsByClassName('fattura24-cb')];

    function toggleElements(select) {
        let tax_rate_id = select.dataset.rowId;
        let button = document.querySelector(`.button.apply-natura[data-row-id='${tax_rate_id}']`);
        let initialValue = select.getAttribute('data-initial-value');


        if (select.value && select.value !== initialValue) {
            button.style.display = 'inline-block';
        } else {
            button.style.display = 'none';
        }
    }

    selectElements.forEach(select => {
        select.setAttribute('data-initial-value', select.value);
        toggleElements(select);
        select.addEventListener('change', () => toggleElements(select))
    });

    function updateCheckboxes(clickedCheckbox) {
        checkboxes.forEach(checkbox => {
            if (checkbox !== clickedCheckbox) {
                checkbox.checked = false;
            }
        });
    }

    checkboxes.forEach(checkbox => {
        checkbox.addEventListener('click', (e) => {
            let rowId = checkbox.dataset.id;
            let button = document.querySelector(`button.apply-natura[data-id='${rowId}']`);
            let select = document.querySelector(`.tax-codes[data-id='${rowId}']`);
            let initialValue = select.getAttribute('data-initial-value');
            updateCheckboxes(e.target);

            if (button && (select.value !== initialValue || checkbox.checked)) {
                button.style.display = 'inline-block';
            } else if (button) {
                button.style.display = 'none';
            }
        });
    });

   

    applyButtons.forEach(button => {
        button.addEventListener('click', (e) => {
            e.preventDefault();
            let tax_id = e.target.dataset.id;
            let tax_rate_id = e.target.dataset.rowId || 0;
            let select = document.querySelector(`.tax-codes[data-row-id='${tax_rate_id}']`);
            let checkbox = document.querySelector(`.fattura24-cb[data-used-for-shipping-id='${tax_rate_id}']`);
            let tax_code = select.value;
          
            if (tax_code) {
                let data = {
                    tax_id: tax_id,
                    row_id: tax_rate_id,
                    tax_code: tax_code,
                    used_for_shipping: checkbox.checked ? '1' : '0',
                    blog_id: f24_scripts_data['blog_id'] || '1',
                };
                updateTaxes(data);
            } else {
                alert('No tax code chosen');
            }    
        });
    });

    function updateTaxes(data) {
         let { row_id, tax_id, tax_code, used_for_shipping, blog_id } = data;

         $.ajax({
            type: 'POST',
            url: f24_scripts_data['url'],
            data:{ 
                action: 'update_tax_code',
                nonce: f24_scripts_data['taxes_nonce'],
                tax_id: tax_id,
                row_id: row_id,
                tax_code: tax_code,
                used_for_shipping: used_for_shipping,
                blog_id: blog_id
            },
            dataType: 'json'
        }).done(function(response) {
            if (response.success) {
                alert(response.data);
                location.reload();
            } else {
                alert('Error: ' + response.data);
            }
        }).fail(function (err) {
            console.log('error :', err, 'arguments :', arguments);
            alert('Error occurred while updating. Please check the console for more details');
        });
    }
});