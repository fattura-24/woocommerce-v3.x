<?php
/**
 * Classe con cui aggiungo la paginazione alla scheda 'Chiamate API'
 * prepara i dati e la tabella che poi sarà visualizzata nel file src/f24_api_info.php
 */
namespace Fattura24;

defined('ABSPATH') || die;


if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Fattura24_API_Calls_Table extends \WP_List_Table {
    private $api_data;
    public $total_items;

    public function __construct() {
        parent::__construct([
            'singular' => 'api_call',
            'plural'   => 'api_calls',
            'ajax'     => false
        ]);
    }

    public function prepare_items() {
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];

        $per_page = 30;
        $current_page = $this->get_pagenum();

        $api_key = get_option(FATT_24_OPT_API_KEY);
        $result = fatt_24_api_call('GetCallLog', [
            'apiKey' => $api_key, 
            'page' => $current_page, 
            'pageSize' => $per_page
        ], FATT_24_API_SOURCE);

        $response = is_array($result) ? json_decode(json_encode($result)) : simplexml_load_string($result);

        if (is_object($response)) {
            $this->api_data = $response->log;
            $this->total_items = (int) $response->totalRecord;
        } else {
            $this->api_data = [];
            $this->total_items = 0;
        }

        $this->set_pagination_args([
            'total_items' => $this->total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($this->total_items / $per_page)
        ]);

        $this->items = $this->api_data;
    }

    public function get_columns() {
        return [
            'date'          => __('Date', 'fattura24'),
            'serviceName'   => __('Command', 'fattura24'),
            'source'        => __('Source', 'fattura24'),
            'deliveryTime'  => __('Response time (ms)', 'fattura24')
        ];
    }

    public function column_default($item, $column_name) {
        return $item->$column_name;
    }

    public function column_deliveryTime($item) {
        return '<div style="text-align:center;">' . $item->deliveryTime . '</div>';
    }
}