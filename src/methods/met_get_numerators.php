<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * Lista modelli elaborata dalla risposta alla chiamata API GetNumerator
 * la chiamata è in api/api_get_numerators;
 *
 * La lista è utilizzata come elenco di opzioni nelle impostazioni del plugin
 */

namespace Fattura24;

if (!defined('ABSPATH')) exit;

require_once FATT_24_CODE_ROOT . 'api/api_wrapper.php';

// lista sezionali in F24
function fatt_24_getSezionale($api_status)
{
  
    if (!$api_status) {
        update_option('numerator_list_1', array('Predefinito' => __('Default', 'fattura24')));
        update_option('numerator_list_3', array('Predefinito' => __('Default', 'fattura24')));
        update_option('numerator_list_11', array('Predefinito' => __('Default', 'fattura24')));
        return;
    }
    
    $idTipiDocumento = [1, 3, 11];
    $f24_sezionali = fatt_24_get_numerators();  
    
    foreach ($idTipiDocumento as $tipo) {
        $listaNomi = array();
        $listaNomi['Predefinito'] = __('Default', 'fattura24');

        if (is_array($f24_sezionali) && $f24_sezionali['code'] !== 200) {
            $message_displayed = $f24_sezionali['disp_message'];
            update_option('numerator_list_' . $tipo, $listaNomi);
            continue;
        }
        $encoding = mb_detect_encoding($f24_sezionali, 'UTF-8, ISO-8859-1, ISO-8859-15, Windows-1252', true);
        if ($encoding === false) {
            $encoding = 'ISO-8859-1';
        }
    
        $f24_sezionali_utf8 = iconv($encoding, 'UTF-8//TRANSLIT', $f24_sezionali);
        $xml = simplexml_load_string($f24_sezionali_utf8);
        libxml_use_internal_errors(true);


        if (is_object($xml) && ($xml !== false)) {
            if (count($xml->sezionale) > 0) {
                unset($listaNomi['Predefinito']);
            }

            foreach ($xml->sezionale as $sezionale) {
                foreach ($sezionale->doc as $doc) {
                    //fatt_24_trace('risultato :', $doc);
                    if ((int)$doc->id == $tipo && (int)$doc->stato == 1) {
                        $listaNomi[(int)$sezionale->id] = (string)$sezionale->anteprima;
                    } else if ((int)$doc->id == $tipo && (int)$doc->stato == 2) {
                        $listaNomi[(int)$sezionale->id] = (string)$sezionale->anteprima . __(' (Default)', 'fattura24');
                    }
                    // visualizza l'anteprima e non l'id per migliore esperienza d'uso
                }
            }
        } else {
            $errors = libxml_get_errors();
            fatt_24_trace('XML parsing errors numerators:', var_export($errors, true));
            libxml_clear_errors();
        }
        
        update_option('numerator_list_' . $tipo, $listaNomi);
    }
}

function fatt_24_get_sezionale_fattura() {
    return get_option('numerator_list_1', ['predefinito' => __('Default', 'fattura24')]);
}

function fatt_24_get_sezionale_ricevuta() {
    return get_option('numerator_list_3', ['predefinito' => __('Default', 'fattura24')]);
}

function fatt_24_get_sezionale_fe() {
    return get_option('numerator_list_11', ['predefinito' => __('Default', 'fattura24')]);
}