<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * Lista modelli elaborata dalla risposta alla chiamata API GetPdc
 * la chiamata è in api/api_get_pdc;
 *
 * La lista è utilizzata come elenco di opzioni nelle impostazioni del plugin
 */

namespace Fattura24;

if (!defined('ABSPATH')) exit;

require_once FATT_24_CODE_ROOT . 'api/api_wrapper.php';

//lista pdc
function fatt_24_getPdc($api_status)
{
    $listaNomi = array();
    $listaNomi['Nessun Pdc'] = __('None', 'fattura24');
    
    if ($api_status) {
        $f24_pdc = fatt_24_get_pdc();

       
        if (!is_array($f24_pdc)) { 
            
            $encoding = mb_detect_encoding($f24_pdc, 'UTF-8, ISO-8859-1, ISO-8859-15, Windows-1252', true);
            if ($encoding === false) {
                $encoding = 'ISO-8859-1';
            }
    
            $f24_pdc_utf8 = iconv($encoding, 'UTF-8//TRANSLIT', $f24_pdc);
            $xml = simplexml_load_string($f24_pdc_utf8);
            libxml_use_internal_errors(true);


            if (is_object($xml) && ($xml !== false)) {
                foreach ($xml->pdc as $pdc) {
                    $listaNomi[(int)$pdc->id] = str_replace('^', '.', (string)$pdc->codice) .
                    ' - ' . str_replace('\'', '\\\'', (string)$pdc->descrizione);
                } // visualizza la descrizione e non l'id per migliore esperienza d'uso
            } else {
                $errors = libxml_get_errors();
                fatt_24_trace('XML parsing errors pdc:', var_export($errors, true));
                libxml_clear_errors();
            }
        }    
    }

    update_option('pdc_list', $listaNomi);
}

function fatt_24_get_pdc_list() {
    return get_option('pdc_list', ['predefinito' => __('Default', 'fattura24')]);
}