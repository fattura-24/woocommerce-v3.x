<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * Lista modelli elaborata dalla risposta alla chiamata API GetTemplate
 * la chiamata è in api/api_get_templates;
 *
 * La lista è utilizzata come elenco di opzioni nelle impostazioni del plugin
 */

namespace Fattura24;

if (!defined('ABSPATH')) exit;

require_once FATT_24_CODE_ROOT . 'api/api_wrapper.php';


//lista templates
function fatt_24_getTemplate($force_check = false)
{
    static $cache = null;

    $listaNomi = array('Predefinito' => __('Default', 'fattura24'));

    if ($cache === null || $force_check) {
        $options = ['order', 'invoice'];
        $templates = fatt_24_get_templates();
        $api_status = !is_array($templates) && $templates !== false;

        // Inizializza con i valori predefiniti
        $listaModelli = array(
            'order' => $listaNomi,
            'invoice' => $listaNomi
        );

        if ($api_status) {

            $encoding = mb_detect_encoding($templates, 'UTF-8, ISO-8859-1, ISO-8859-15, Windows-1252', true);
            if ($encoding === false) {
                $encoding = 'ISO-8859-1';
            }

            $templates_utf8 = iconv($encoding, 'UTF-8//TRANSLIT', $templates);
            $xml = simplexml_load_string($templates_utf8);

            libxml_use_internal_errors(true);


            if (is_object($xml) && ($xml !== false)) {
                foreach ($xml->modelloOrdine as $modello) {
                    $listaModelli['order'][(int)$modello->id] = str_replace('\'', '\\\'', (string)$modello->descrizione) . " (ID: " . (int)$modello->id . ")";
                }
                foreach ($xml->modelloFattura as $modello) {
                    $listaModelli['invoice'][(int)$modello->id] = str_replace('\'', '\\\'', (string)$modello->descrizione) . " (ID: " . (int)$modello->id . ")";
                }
            } else {
                $errors = libxml_get_errors();
                fatt_24_trace('XML parsing errors templates:', var_export($errors, true));
                libxml_clear_errors();
            }

            update_option('template_list_order', $listaModelli['order']);
            update_option('template_list_invoice', $listaModelli['invoice']);
        } else {
            update_option('template_list_order', $listaNomi);
            update_option('template_list_invoice', $listaNomi);
        }

        $cache = array('api_status' => $api_status, 'templates' => $templates);
    }

    return $cache;
}


/**
 * In case of fresh install I didn't put yet my API Key
 * so I fill my array of results with default value, otherwise
 * a fatal error should occur in loading settings. Davide Iandoli 26.08.2024
 * Edit 11.09.2024 now I use $default_value param, 
 * see here : https://developer.wordpress.org/reference/functions/get_option/
 */
function fatt_24_get_template_order() {
    return get_option('template_list_order', ['predefinito' => __('Default', 'fattura24')]);
}

/**
 * In case of fresh install I didn't put yet my API Key
 * so I fill my array of results with default value, otherwise
 * a fatal error should occur in loading settings. Davide Iandoli 26.08.2024
 */

function fatt_24_get_template_invoice() {
    $resultList = empty(get_option(FATT_24_OPT_API_KEY)) ? 
        ['predefinito' => __('Default', 'fattura24')] : get_option('template_list_invoice');
    return $resultList;
}