<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * metodi utilizzati da hooks.php correlati alla gestione
 * e all'aggiunta dei campi fiscali - lato utente, lato checkout, lato admin
 *
 */

namespace Fattura24;

if (!defined('ABSPATH')) {
    exit;
}

function fatt_24_f24_added_vat_field() {
    return FATT_24_PLUGIN_DATA['Name'] == get_option('fatt-24-add-vat-field');
}

function fatt_24_show_billing_checkbox()
{
    if ('2' == get_option('fatt-24-inv-create') && fatt_24_get_flag(FATT_24_INV_DISABLE_RECEIPTS)) {
        return false;
    }

    $showCheckbox = get_option('fatt-24-toggle-billing-fields') == '';
    $piRequired = fatt_24_get_flag(FATT_24_ABK_VATCODE_REQ) == 1;
    $result = true;

    if ($piRequired || !$showCheckbox) {
        $result = false;
    }

    return $result;
}

function fatt_24_delete_user($user_id)
{
    $f24UserMetaArray = ['billing_fiscalcode', 'billing_vatcode', 'billing_pecaddress', 'billing_recipientcode'];
    foreach ($f24UserMetaArray as $metakey) {
        delete_user_meta($user_id, $metakey);
    }
}

/**
 * Campi aggiunti al checkout
 * layout tradizionale
 */
function fatt_24_billing_checkout_fields($fields)
{
    $showCheckbox = fatt_24_show_billing_checkbox();
    $addedF24PIVA = fatt_24_f24_added_vat_field(); 

    if ($showCheckbox) {
        $fields['billing']['billing_checkbox'] = array(
            'type'     => 'checkbox',
            'label'    => __('I would like to receive an invoice', 'fattura24'),
            'class'    => array('form-row-wide', 'tablecell', 'fatt_24_billing_cb'),
            'priority' => 25,
            'clear'    => true
        );
    }
    
    if ($addedF24PIVA) {
        $fields['billing']['billing_vatcode'] = array(
                'type'       => 'text',
                'placeholder' => __('VAT Number', 'fattura24'),
                'label'      => __('VAT Number', 'fattura24'),
                'class'      => array('form-row-wide'),
                'priority'   => 30,
                'clear'      => true,
            );
    }    

    $fields['billing']['billing_fiscalcode'] = array(
        'type'       => 'text',
        'placeholder' => __('Fiscal code', 'fattura24'),
        'label'      => __('Fiscal code', 'fattura24'),
        'class'      => array('form-row-wide', 'fattura24'),
        'priority'   => 31,
        'clear'      => true
    );

    $fields['billing']['billing_recipientcode'] = array(
            'type' => 'text',
            'placeholder' => __('SDI Code', 'fattura24'),
            'label' => __('SDI Code', 'fattura24'),
            'maxlength' => 7, // definisco la lunghezza massima dell'input
            'class' => array('form-row-wide', 'fattura24'),
            'priority' => 32, // cambio l'ordine dei campi nel layout
            'clear' => true
        );

    $fields['billing']['billing_pecaddress'] = array(
             'type' => 'email',
             'placeholder' => __('Indirizzo PEC', 'fattura24'),
             'label' => __('Indirizzo PEC', 'fattura24'),
             'validate' => array('email'),
             'class' => array('form-row-wide', 'fattura24'),
             'priority' => 33,
             'clear' => true
        );

    return $fields;
}

/**
 * I campi sono gli stessi già settati sopra,
 * la checkbox deve essere visibile solo nel checkout
 */
function fatt_24_billing_fields($fields)
{
    global $wp;
    
    $isCheckout = is_checkout();
    $f24BillingFields = fatt_24_billing_checkout_fields($fields)['billing'];
    $showCheckbox = fatt_24_show_billing_checkbox();
    //fatt_24_trace('billing fields 1 :', $f24BillingFields);

    if ($showCheckbox && !$isCheckout) {
        //array_shift($f24BillingFields);
        unset($f24BillingFields['billing_checkbox']);
    }

    //fatt_24_trace('campi di fatturazione dopo il controllo:', $f24BillingFields);


    foreach ($f24BillingFields as $key => $field) {
        $fields[$key] = $field;
    }

    return $fields;
}

function fatt_24_checkout_fields_validation($data, $errors)
{
    global $woocommerce;

    $customer = $woocommerce->customer ? $woocommerce->customer : new \WC_Customer(0, true);
    $country = method_exists($customer, 'get_billing_country') ? $customer->get_billing_country() : 
            $customer->get_country(); // deprecato da WooCommerce 3.0

    $billing_vatcode = '';
    if (isset($data['billing_vatcode'])) {
        $billing_vatcode = trim($data['billing_vatcode']);
    } else if (isset($_POST['$billing_vatcode'])) {
        $billing_vatcode = trim($_POST['billing_vatcode']);
    }        

    if ($country !== 'IT') {
        $billing_fiscalcode = '';
    } else {
        $billing_fiscalcode = isset($data['billing_fiscalcode']) ? trim($data['billing_fiscalcode']) : trim($_POST['billing_fiscalcode']);
    }      

    $customerRequiredInvoice = isset($_POST['billing_checkbox']) && (int) $_POST['billing_checkbox'] == 1;
        
    $message = fatt_24_get_validation_message($billing_fiscalcode, $billing_vatcode, $country, $customerRequiredInvoice);
    //fatt_24_trace('message :', $message);
    
    if ($message) {
        $errors->add('validation', $message);
    }
}

function fatt_24_add_fieldset_to_my_account()
{
    if (fatt_24_customer_use_cf() || fatt_24_customer_use_vat()) {
        $user_id = get_current_user_id();
        $user = get_userdata($user_id); ?>
    
            <fieldset>
                <legend>
                    <?php _e('Dati fiscali', 'fattura24') ?> <!-- edited label Davide Iandoli 29.01.2019 -->
                </legend>
                <?php if (fatt_24_customer_use_cf()) { ?>
                    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide"> <!-- edited style -->
                        <label for="billing_fiscalcode">
                            <?php __('Codice Fiscale', 'fattura24') ?>
                        </label>
    
                        <input type="text" class="woocommerce-Input input-text"
                               name="billing_fiscalcode" id="billing_fiscalcode"
                               value="<?php echo esc_attr(strtoupper(fatt_24_user_fiscalcode($user_id))) ?>" />
    
                        <span class="description">
                            <?php _e('inserisci un codice fiscale valido', 'fattura24') ?> <!-- edited label Davide Iandoli 29.01.2019 -->
                        </span> 
                    </p>
                <?php } ?>
         
                <?php if (fatt_24_customer_use_vat()) { ?>
                    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                        <label for="billing_vatcode">
                            <?php _e('Partita Iva', 'fattura24')?>
                        </label>
    
                        <input type="text" class="woocommerce-Input input-text"
                               name="billing_vatcode" id="billing_vatcode"
                               value="<?php echo esc_attr(fatt_24_user_vatcode($user_id)) ?>" />
    
                        <span class="description">
                            <?php _e('inserisci una Partita Iva valida', 'fattura24') ?> <!-- edited label Davide Iandoli 29.01.2019 -->
                        </span> 
                    </p>
                  <?php } ?> 
                    <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                        <label for="billing_recipientcode">
                            <?php _e('Codice Destinatario', 'fattura24')?>
                        </label>
    
                        <input type="text" class="woocommerce-Input input-text"
                               name="billing_recipientcode" id="billing_recipientcode"
                               maxlength="7"
                               value="<?php echo esc_attr(fatt_24_user_recipientcode($user_id)) ?>" />
    
                        <span class="description">
                            <?php _e('inserire un codice destinatario valido') ?> 
                            <p class="woocommerce-FormRow woocommerce-FormRow--wide form-row form-row-wide">
                        
                        <label for="billing_pec">
                            <?php _e('Indirizzo PEC', 'fattura24')?>
                        </label>
            
                        <input type="email" class="woocommerce-Input input-text"
                               name="billing_pecaddress" id="billing_pecaddress"
                               value="<?php echo esc_attr(fatt_24_user_pecaddress($user_id)) ?>" />
               
                        <span class="description">
                            <?php _e('inserire un indirizzo PEC valido', 'fattura24'); ?> 
            </fieldset> 
    
        <div class="clear"></div>
    <?php
    }
}

function fatt_24_customer_meta_fields($fields)
{
    if (fatt_24_customer_use_cf() || fatt_24_customer_use_vat()) {
        if (!isset($fields['fatt_24'])) {
            $fields['fatt_24'] = array(
                'title' => __('Invoicing required fields', 'fattura24'),
                'fields' => array()
            );
        }
        if (fatt_24_customer_use_cf()) {
            $fields['fatt_24']['fields']['billing_fiscalcode'] = array(
                'label' => __('Fiscal Code', 'fattura24'),
                'description' => __('a valid Fiscal Code', 'fattura24'),
            );
        }
        if (fatt_24_customer_use_vat()) {
            $fields['fatt_24']['fields']['billing_vatcode'] = array(
                'label' => __('Vat code', 'fattura24'),
                'description' => __('a valid Vat Code', 'fattura24'),
            );
        }
    }

    if (fatt_24_customer_use_recipientcode()) {
        $fields['fatt_24']['fields']['billing_recipientcode'] = array(
            'label' => __('SDI Code', 'fattura24'),
            'description' => __('a valid SDI Code', 'fattura24'),
        );

        $fields['fatt_24']['fields']['billing_pecaddress'] = array(
            'label' => __('Indirizzo PEC', 'fattura24'),
            'description' => __('a valid PEC address', 'fattura24'),
        );
    }
    return $fields;
}

function fatt_24_created_customer($customer_id)
{
    $f24billingFields = ['billing_fiscalcode', 'billing_vatcode', 'billing_pecaddress', 'billing_recipientcode'];

    foreach ($f24billingFields as $field) {
        if (isset($_POST[$field])) {
            update_user_meta($customer_id, $field, sanitize_text_field($_POST[$field]));
        }
    }
}

function fatt_24_save_account_details($user_id)
{
    if (fatt_24_customer_use_cf()) {
        update_user_meta($user_id, 'billing_fiscalcode', htmlentities(sanitize_text_field(strtoupper($_POST['billing_fiscalcode']))));
    }

    if (fatt_24_customer_use_vat()) {
        update_user_meta($user_id, 'billing_vatcode', htmlentities(sanitize_text_field($_POST['billing_vatcode'])));
    }

    if (fatt_24_customer_use_recipientcode()) {
        update_user_meta($user_id, 'billing_recipientcode', htmlentities(sanitize_text_field($_POST['billing_recipientcode'])));
        update_user_meta($user_id, 'billing_pecaddress', htmlentities(sanitize_text_field($_POST['billing_pecaddress'])));
    }
}

function fatt_24_checkout_meta($order_id, $data) {
   
    $order = new \WC_Order($order_id);

    if (0 === $order_id) {
        fatt_24_trace('Potenziale errore: l\'ordine potrebbe non essere stato salvato perché l\'id è zero !');
    }

    $order->add_meta_data(FATT_24_ORDER_INVOICE_STATUS, '');
    $f24billingFields = ['billing_checkbox', 'billing_fiscalcode', 'billing_pecaddress', 'billing_recipientcode'];
    $addedF24PIVA = fatt_24_f24_added_vat_field() == true;
    
    if ($addedF24PIVA) {
        array_push($f24billingFields, 'billing_vatcode');
    }

    foreach ($f24billingFields as $field) {
        if (!empty($data[$field])) {
            $order->add_meta_data("_$field", $data[$field]);
        }
    } 
}

/** Gestione del checkout dell'ordine layout classico */

/**
 * Metodo con cui mostro o nascondo i campi aggiuntivi in base ad alcune opzioni
 * oppure in base al paese di fatturazione selezionato
 * edit 03.03.2022 : ora richiamo js esterni a cui passo i dati necessari
 * edit 18.04.2024 : sostituiti i due file js con un unico file
 */
function fatt_24_manage_checkout()
{
    $error_message_template = __('%1s required in lack of %2s', 'fattura24');
    $vat_message = sprintf($error_message_template, __('VAT Number', 'fattura24'), __('Fiscal code', 'fattura24'));
    $cf_message = sprintf($error_message_template, __('Fiscal code', 'fattura24'), __('VAT Number', 'fattura24'));
    $addJs = 'f24_checkout_fields';

    $data = [
             'alwaysCreateFE' => get_option('fatt-24-inv-create') == '2' && fatt_24_get_flag(FATT_24_INV_DISABLE_RECEIPTS),
             'showCheckbox' => fatt_24_show_billing_checkbox(),
             'cfRequired' => fatt_24_CF_flag() == 1,
             'pIRequired' => fatt_24_get_flag(FATT_24_ABK_VATCODE_REQ) == 1,
             'vat_message' => $vat_message,
             'cf_message' => $cf_message
            ];

     
    wp_enqueue_script($addJs, fatt_24_url('/js/checkout/'. $addJs . '.js'), array('jquery'));
    wp_localize_script($addJs, 'f24_checkout_vars', $data);

    // added for debug purposes
    $script_list = implode(', ', [$addJs]);        
    wp_add_inline_script($addJs, "console.log('Scripts loaded on checkout page: {$script_list}');", 'after');   
}

/** 
* Per la logica di convalida checkout devo controllare codice fiscale e/o partita iva
* lista controlli: (layout shortcodes)
*
* C.F. :
* - se $country == 'IT';
* - se il campo è obbligatorio (admin o frontend) e vuoto
* - se il campo p. iva NON è vuoto
* - se la lunghezza della stringa è 11 oppure 16;
*
* P. IVA : 
* - se il campo è aggiunto e gestito da F24;
* - se il campo è obbligatorio (admin o frontend);
* - se il campo c.f. NON è vuoto
* - lunghezza uguale a 11 solo se il paese è IT (altrimenti niente controllo sulla lunghezza)
* ATTENZIONE: se l'opzione selezionata in Crea doc fiscale è 2 e il paese !== IT forzo la compilazione del campo p.iva con 
* nome / ragione sociale
*
*/


function fatt_24_get_validation_message($billing_fiscalcode, $billing_vatcode, $country, $customerRequiredInvoice) 
{
    $message = '';
    $checkCF = fatt_24_should_check_cf($billing_fiscalcode, $billing_vatcode, $country, $customerRequiredInvoice);
    $checkPI = fatt_24_should_check_pi($billing_fiscalcode, $billing_vatcode, $country, $customerRequiredInvoice);
    $bothCheck = $checkCF && $checkPI;

    
    fatt_24_trace('checkCF :', var_export($checkCF, true));
    fatt_24_trace('checkPI :', var_export($checkPI, true));
    fatt_24_trace('bothCheck :', var_export($bothCheck, true));
    
    
    /**
     * Se è obbligatorio inserire uno dei due campi controllo se entrambi sono vuoti
     * se è vuota solo la p.iva controllo il contenuto del campo c.f., altrimenti controllo il campo p.i
     */
    if ($bothCheck && empty($billing_fiscalcode) && empty($billing_vatcode)) {
        $message = __('You should fill either Fiscal code or Vat code field', 'fattura24');

    } else if ($bothCheck && empty($billing_vatcode)) {
        $message = fatt_24_get_cf_validation_message($billing_fiscalcode);

    } else if ($bothCheck && empty($billing_fiscalcode)) {
        $message = fatt_24_get_pi_validation_message($billing_vatcode, $country);
    } 
    // fatt_24_trace('message se $bothCheck è vera :', $message);
    

    fatt_24_trace('message è vuoto ? :', var_export($message, true));
    
    if (!empty($message)) {
        return $message;
    }

    if ($checkCF) {
        $message = fatt_24_get_cf_validation_message($billing_fiscalcode);
    }

   
    if ($checkPI) {
        $message = fatt_24_get_pi_validation_message($billing_vatcode, $country);
    }

    fatt_24_trace('message alla fine :', var_export($message, true));
    return $message;
}


/**
 * The function `fatt_24_should_check_cf` is returning a boolean value based on the conditions
 * provided. If the country is 'IT' and the billing VAT code is empty, it will return true if any of
 * the following conditions are met:
 * 1. The `fatt_24_CF_flag()` function returns true.
 * 2. The `$customerRequiredInvoice` variable is true.
 * 3. The documentType is Fattura Elettronica
 */
function fatt_24_should_check_cf($billing_fiscalcode, $billing_vatcode, $country, $customerRequiredInvoice) 
{
    /*
    * Nel caso in cui il campo p.iva è opzionale e compilato
    * ritorna false anche se visivamente il campo è contrassegnato come obbligatorio
    */
   
    if (!empty($billing_vatcode)) {
        return false;
    }
    
    if ($country == 'IT' && fatt_24_f24_added_vat_field()) {
        return fatt_24_CF_flag() || 
               $customerRequiredInvoice || 
               fatt_24_get_resulting_doc_type($country) == FATT_24_DT_FATTURA_ELETTRONICA;
    }
    return false;
}

/**
 * The function checks if certain conditions are met to determine if the PI (electronic invoice) should
 * be checked.
 * 
 * @param $billing_fiscalcode The `billing_fiscalcode` parameter is a variable that stores the
 * fiscal code of the billing entity or individual. In the provided function `fatt_24_should_check_pi`,
 * this parameter is being checked for emptiness using the `empty()` function.
 * @param $billing_vatcode The `billing_vatcode` parameter is a code that identifies the VAT
 * (Value Added Tax) registration number of the customer or business entity. It is used for tax
 * purposes and is often required on invoices and other financial documents.
 * @param $country The `country` parameter in the function `fatt_24_should_check_pi` is used to specify
 * the country for which the function is checking certain conditions.
 * @param $customerRequiredInvoice The `customerRequiredInvoice` parameter is a boolean value that
 * indicates whether the customer requires an invoice. It is used as a condition in the
 * `fatt_24_should_check_pi` function to determine if certain criteria are met for checking the PI
 * (Partita IVA) field in the billing information
 * 
 * @return boolean value based on the conditions provided.
 */
function fatt_24_should_check_pi($billing_fiscalcode, $billing_vatcode, $country, $customerRequiredInvoice) 
{
    /** 
     * Non devo passare il controllo di validità sul campo p.iva  
     * se $invoiceCreateOption è fattura elettronica, $country NON è IT e il campo p. iva è vuoto
     * In quel caso infatti forzo la compilazione del campo con nome/cognome
     * oppure ragione sociale (cfr. api_call.php riga 248)
     * 
     * Davide Iandoli 11.01.2023 
     * Edit del 30.04.2024 - blocco di codice inserito nel primo caso della switch
     */
    
    
    $invoiceCreateOption = (string) get_option('fatt-24-inv-create'); 
    $result = false; // valore predefinito
     
    // se è compilato il codice fiscale non serve controllare la p.iva
    if (!empty($billing_fiscalcode)) {
        return $result;
    }
    
    // faccio i controlli solo se il campo è gestito e aggiunto da Fattura24
    switch (fatt_24_f24_added_vat_field()) {
        // caso in cui forzo la compilazione del campo p. IVA anche in assenza di un dato
        case $invoiceCreateOption == '2' && 
            $country !== 'IT' && 
            empty($billing_vatcode) &&
            !$customerRequiredInvoice:

            // non faccio nulla ed esco dalla switch: deve restituire false
            break;

        // se da admin il campo è obbligatorio o il cliente ha richiesto la fattura devo fare il controllo
        /**
         * Qui ci arrivo solo se il campo billing fiscalcode è vuoto, per questo ho commentato il codice sotto
         * sarà rimosso se non serve - Davide Iandoli 18.09.2024
         */
        case fatt_24_get_flag(FATT_24_ABK_VATCODE_REQ) == 1:
        case $customerRequiredInvoice:  
 
             $result = true;
            break;    

        /* case $customerRequiredInvoice:
            if ($country !== 'IT' || (empty($billing_fiscalcode) && $country == 'IT')) {
                $result = true;
            }
            break;
        */
        case fatt_24_get_resulting_doc_type($country) == FATT_24_DT_FATTURA_ELETTRONICA:
            $result = true;   
 
            break;

        default:
        break;
    }

    return $result;
}

/**
 * The function checks the validity of a billing fiscal code and returns an error message if it is
 * empty or not of the required length.
 * 
 * @param $billing_fiscalcode The function `fatt_24_get_cf_validation_message` is used to validate the
 * billing fiscal code provided as a parameter. It checks if the fiscal code is empty, the length of
 * the fiscal code, and returns an appropriate validation message based on the conditions.
 * 
 * @return '' string is being returned if the billing fiscal code is not empty and has a length
 * of either 11 or 16 characters. If the billing fiscal code is empty or does not have a valid length,
 * a corresponding error message is returned.
 */
function fatt_24_get_cf_validation_message($billing_fiscalcode) {
    
    if (empty($billing_fiscalcode)) {
        return __('Fiscal code required', 'fattura24');
    }

    $cfLen = strlen($billing_fiscalcode);
    if (!in_array($cfLen, [11, 16])) {
        return __('Fiscal code should be long 11 or 16 characters', 'fattura24');
    }
    return ''; // No error
}

/**
 * The function `fatt_24_get_pi_validation_message` validates a billing VAT code in PHP and returns an
 * error message if the code is empty or not 11 characters long.
 * 
 * @param $billing_vatcode The function `fatt_24_get_pi_validation_message` is used to validate a
 * billing VAT code. It checks if the VAT code is empty or not 11 characters long and returns an error
 * message accordingly.
 * 
 * @return string `` is  empty, the function will return 'Vat code required'. If the
 * length of `` is not 11 characters, the function will return 'Vat code should be long
 * 11 characters'. Otherwise, if there are no errors, an empty string will be returned.
 */
function fatt_24_get_pi_validation_message($billing_vatcode, $country)
{
    // controllo aggiuntivo, vedi ticket n.: 30208
    if (!fatt_24_f24_added_vat_field()) {
        return '';
    }
    
    if (empty($billing_vatcode)) {
        return __('Vat code required', 'fattura24');
    }

    if ($country == 'IT' && strlen($billing_vatcode) !== 11) {
        return __('Vat code should be long 11 characters', 'fattura24');
    }

    return ''; // No error
}

/** Fine blocchi di codice gestione del checkout dell'ordine layout classico */