<?php
/**
 * Gestione delle tipologie di pagamento (codici MP), logica di business 
 * per ogni gateway (es.: Stripe, Stripe SEPA, ...) aggiungo un'opzione specifica
 * con cui associo il codice MP al gateway
 * con chiamate AJAX le aggiorno
 * la parte visiva è in src/payment_types.php
 */
namespace Fattura24;

defined('ABSPATH') || die;

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}


class Fattura24_Payment_Types extends \WP_List_Table {
    private $items_data;
    private $total_items;
    private $payment_groups;
    private $electronic_payments;

    public function __construct(){
        parent::__construct([
                'singular' => 'payment_type',
                'plural' => 'payment_types',
                'ajax' => false
        ]);    

        $this->items_data = WC()->payment_gateways->get_available_payment_gateways();
        $this->total_items = count($this->items_data);
        $this->electronic_payments = $this->get_electronic_payments();
        $this->payment_groups = $this->get_payment_groups();

        $this->init_payment_types();
    }

    private function get_electronic_payments() {
        return [
            'paypal',
            'ppcp',
            'braintree',
            'stripe',
            'ppay',
            'payplug',
            'vivawallet',
            'accountfunds'
        ];
    }

    private function get_payment_groups()
    {
        return array(
            'MP01' => array('cod'),
            'MP02' => array('cheque'),
            'MP05' => array('bacs'),
            'MP08' => $this->electronic_payments
        );
    }

    private function get_payment_type_options()
    {
        return [
            'MP01' => __('Cash on delivery - MP01', 'fattura24'),
            'MP02' => __('Cheque - MP02', 'fattura24'),
            'MP05' => __('Bank transfer - MP05', 'fattura24'),
            'MP07' => __('Bank bill - MP07', 'fattura24'),
            'MP08' => __('Credit card - MP08', 'fattura24'),
            'MP09' => __('RID - MP09', 'fattura24'),
            'MP10' => __('RID utility - MP10', 'fattura24'),
            'MP11' => __('RID fast - MP11', 'fattura24'),
            'MP17' => __('Postal direct debit - MP17', 'fattura24'),
            'MP19' => __('SEPA Direct Debit - MP19', 'fattura24'),
            'MP20' => __('SEPA Direct Debit CORE - MP20', 'fattura24'),
            'MP21' => __('SEPA Direct Debit B2B - MP21', 'fattura24')
        ];
    }

    private function init_payment_types() {
        foreach ($this->items_data as $gateway) {
            $gateway_id = $gateway->id;
            $current_type = get_option('fatt_24_payment_type_' . $gateway_id, '');
            
            if (empty($current_type)) {
                $default_type = $this->get_default_payment_type($gateway_id);
                update_option('fatt_24_payment_type_' . $gateway_id, $default_type);
            }
        }
    }

    private function get_default_payment_type($gateway_id) {
        foreach ($this->payment_groups as $type => $gateways) {
            if (in_array($gateway_id, $gateways)) {
                return $type;
            }
        }
        return 'MP08'; // Default to credit card if not found
    }

    public function prepare_items() {
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];

        // riordino gli elementi per id crescente (a-z)
        usort($this->items_data, function($a, $b) {
            return strcmp($a->id, $b->id);
        });

        $per_page = 30;
        $current_page = $this->get_pagenum();
            
        $this->set_pagination_args([
            'total_items' => $this->total_items,
            'per_page'    => $per_page,
            'total_pages' => ceil($this->total_items / $per_page)
        ]);

        $this->items = array_slice($this->items_data, ($current_page - 1) * $per_page, $per_page);
    }

    public function get_columns() {
        return[
                'id'          => '<span class="fatt-24-gateway-id">' . __('ID Gateway', 'fattura24') . '</span>',
                'description' => __('Description', 'fattura24'),
                'type'        => __('Type', 'fattura24'),
                'action'     => ''
        ];
    }

    public function column_default($item, $column_name) {
        switch($column_name) {
            case 'id':
                return sprintf('<span class="fatt-24-gateway-id">%s</span>', esc_html($item->id));
            case 'description':
                return $item->title;
            case 'type':
                $selected = get_option('fatt_24_payment_type_' . $item->id, '');
                $is_fixed = $this->is_fixed_payment_type($item->id);
                $element = $is_fixed && $selected ? 
                    $this->get_payment_type_options()[$selected] : 
                    $this->get_payment_type_select($item->id, $selected);
                return $element;
            case 'action':
                $is_fixed = $this->is_fixed_payment_type($item->id);
                if ($is_fixed) {
                    return '';
                } else {
                    return sprintf(
                        '<button class="button apply-type" data-id="%s">%s</button>',
                        esc_attr($item->id),
                        __('Apply', 'fattura24')
                    );
                }
            default: 
                return print_r($item, true);
        }
    }

    private function is_fixed_payment_type($gateway_id) {
        foreach($this->payment_groups as $gateways) {
            if (in_array($gateway_id, $gateways)) {
                return true;
            }
        }
        return false;
    }

   
    private function get_payment_type_select($gateway_id, $selected = '')
    {
        $options = $this->get_payment_type_options();
        $is_fixed = false;
        $fixed_type = '';
        $html = '';

        foreach ($this->payment_groups as $type => $gateways) {
            if (in_array($gateway_id, $gateways)) {
                $is_fixed = true;
                $fixed_type = $type;
                break;
            }
        }
      
        if ($is_fixed) {
            $gateway_option = get_option('fatt_24_payment_type_' . $gateway_id);
            if (!$gateway_option) {
                update_option('fatt_24_payment_type_' . $gateway_id, $fixed_type);
            }
        } else {
            $has_stored_value = get_option('fatt_24_payment_type_' . $gateway_id) !== false;
            $html = "<select name='payment-type-{$gateway_id}' id='payment-type-{$gateway_id}' class='payment-types' data-id='{$gateway_id}' data-has-stored-value='{$has_stored_value}'>";
            $html .= sprintf(
                "<option value='MP08'%s>%s</option>",
                selected($selected, 'MP08', false),
                esc_html($options['MP08'])
            );

            foreach ($options as $value => $label) {
                if ($value !== 'MP08') {
                    $html .= sprintf(
                        "<option value='%s'%s>%s</option>",
                        esc_attr($value),
                        selected($selected, $value, false),
                        esc_html($label)
                    );
                }
            }
        }    
        $html .= "</select>";
        return $html;
    }
}

/**
 * Attenzione: il parametro della funzione è una stringa
 * ma per ottenere le informazioni (es.: title) ho bisogno di un oggetto,
 * per questo ho aggiunto queste due righe:
 * 
 * $payment_gateways = WC()->payment_gateways->payment_gateways();
 * $payment_method = isset($payment_gateways[$payment_method_id]) ? $payment_gateways[$payment_method_id] : null;
 */
function fatt_24_get_payment_info($payment_method_id) {
    $payment_gateways = WC()->payment_gateways->payment_gateways();
    $payment_method = isset($payment_gateways[$payment_method_id]) ? $payment_gateways[$payment_method_id] : null;
    
    $payment_method_title = $payment_method ? $payment_method->get_title() : '';
    $fePaymentCode = '';
    // per default utilizzo $payment_method_title 
    $PaymentMethodName = sprintf('%s - %s', $payment_method_title, $payment_method_id);
    $PaymentMethodDescription = '';
    
    /**
     * Ticket n. 32045 - il controllo ora avviene sul nome del metodo e non sull'oggetto
     */
    switch ($payment_method_id) {
        case 'bacs':
            $fePaymentCode = 'MP05';
            $defaultPaymentMethodName = 'Bonifico bancario';
            
            if (empty($PaymentMethodName)) {
                $PaymentMethodName = $defaultPaymentMethodName;
            }

            $bacs_accounts1 = get_option('woocommerce_bacs_accounts');
        
            if (!empty($bacs_accounts1)) {
                foreach ($bacs_accounts1 as $bacs_account1) {
                    $PaymentMethodName = !empty($bacs_account1['bank_name']) ? $bacs_account1['bank_name']: $defaultPaymentMethodName;
                    $PaymentMethodDescription = $bacs_account1['iban'];
                }
            }
            break;

        case 'cheque':
            $fePaymentCode = 'MP05';
            if (empty($PaymentMethodName)) {
                $PaymentMethodName = 'Assegno';
            } 
            $PaymentMethodDescription = $PaymentMethodName;
            break;    
        
        case 'cod':
            $fePaymentCode = 'MP01';
            if (empty($PaymentMethodName)) {
                $PaymentMethodName = 'Pagamento alla consegna';
            } 
            $PaymentMethodDescription = $PaymentMethodName;
            break;
       
        default:
            $fePaymentCode = 'MP08';
            $PaymentMethodName = ucfirst($payment_method_title);
            $PaymentMethodDescription = $PaymentMethodName;
            break;
    }
    
    return [
        'fe_payment_code' => $fePaymentCode,
        'payment_method_name' => $PaymentMethodName,
        'payment_method_description' => $PaymentMethodDescription
    ];
}

// azione ajax di salvataggio codice mp in wp_options
add_action('wp_ajax_save_mp', __NAMESPACE__ . '\fatt_24_save_payment_type');

// ATTENZIONE: la chimata ajax passato qui sora sta nel file f24_payment_types.js (cartellasrc/js)

function fatt_24_save_payment_type() {
    if (!current_user_can('manage_options')) {
        wp_send_json_error('Permission denied');
        return;
    }

    if (!wp_verify_nonce(sanitize_text_field($_POST['nonce']), 'payment_types_nonce')){
        wp_send_json_error('Invalid nonce');
        return;
    }


    $gateway_id = isset($_POST['gateway_id']) ? (string) sanitize_text_field($_POST['gateway_id']): '';
    $type = isset($_POST['type'])? (string) sanitize_text_field($_POST['type']) : '';

    if ($gateway_id && $type) {
        update_option('fatt_24_payment_type_' . $gateway_id, $type);
        wp_send_json_success(__('Payment type updated successfully', 'fattura24'));
    } else {
        wp_send_json_error(__('Invalid gateway ID or type', 'fattura24'));
    }    
}