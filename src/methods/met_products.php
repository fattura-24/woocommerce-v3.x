<?php
/**
 * Gestione delle voci di ricavo, logica di business 
 * Chiamate AJAX con cui aggiorno il metadato 'pdc' all'interno del prodotto
 * la parte visiva è in src/payment_types.php
 */
namespace Fattura24;

defined('ABSPATH') || die;


if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Fattura24_PDC_List_Table extends \WP_List_Table {
    private $api_status;
    private $pdc_list;

    public function __construct() {
        parent::__construct([
            'singular' => __('Product', 'fattura24'),
            'plural'   => __('Products', 'fattura24'),
            'ajax'     => false
        ]);
        
        $templates_result = fatt_24_getTemplate(true);
        $this->api_status = $templates_result['api_status'];
        $this->pdc_list = fatt_24_getPdc($this->api_status);
    }
        
    public function prepare_items() {
        $columns = $this->get_columns();
        $fatt_24_hidden = array();
        $sortable = $this->get_sortable_columns();
        
        $this->_column_headers = array($columns, $fatt_24_hidden, $sortable);
        
        $per_page = 20;
        $current_page = $this->get_pagenum();
        $total_items = wc_get_products(array('limit' => -1, 'return' => 'ids'));

        $this->set_pagination_args(array(
            'total_items' => count($total_items),
            'per_page' => $per_page
        ));

        $this->items = wc_get_products(array(
            'limit' => $per_page,
            'page' => $current_page
        ));
    }

    public function get_columns() {
        return array(
            'cb' => '<input class="davide" type="checkbox" />',
            'name' => __('Product','fattura24'),
            'pdc' => __('Revenue item', 'fattura24'),
            'action' => ''
        );
    }

    /**
     * Popolo la select con l'elenco dei pdc
     */
    public function get_bulk_actions() {
        $pdc_list = get_option('pdc_list');
        $actions = [];
        foreach ($pdc_list as $pdc_id => $pdc_name) {
            $actions[$pdc_id] = $pdc_name;
        }
        return $actions;
    }

    /**
     * Con questo metodo sovrascrivo l'etichetta 'Azioni di gruppo'
     * con una personalizzata
     */
    protected function bulk_actions($which = '') {
        if ( is_null( $this->_actions ) ) {
            $this->_actions = $this->get_bulk_actions();
            $two            = '';
        } else {
            $two = '2';
        }

        if ( empty( $this->_actions ) )
            return;

        echo '<label for="bulk-action-selector-' . esc_attr( $which ) . '" class="screen-reader-text">' . __( 'Select bulk action' ) . '</label>';
        echo '<select name="bulk-action' . $two . '" id="bulk-action-selector-' . esc_attr( $which ) . "\">\n";
        echo '<option value="-1">' . __( 'Choose item for selected group', 'fattura24' ) . "</option>\n";

        foreach ( $this->_actions as $name => $title ) {
            $class = 'edit' === $name ? ' class="hide-if-no-js"' : '';

            echo "\t" . '<option value="' . $name . '"' . $class . '>' . $title . "</option>\n";
        }

        echo "</select>\n";

        submit_button( __( 'Apply', 'fattura24' ), 'action', '', false, array( 'id' => "doaction$two" ) );
        echo "\n";
    }

    public function process_bulk_action() {
        if ('bulk-update' === $this->current_action()) {
            $product_ids = isset($_POST['product']) ? $_POST['product']: array();
            $pdc = isset($_POST['action']) ? $_POST['action'] : '';

            if (!empty($product_ids) && !empty($pdc)) {
                foreach($product_ids as $product_id) {
                    update_post_meta($product_id, 'pdc', $pdc);
                }
            }
        }
    }

    public function column_default($item, $column_name) {
        switch ($column_name) {
            case 'name':
                return $item->get_name();
            case 'pdc':
                $pdc_list = get_option('pdc_list');
                $current_pdc = get_post_meta($item->get_id(), 'pdc', true);
                $select = '<select class="pdc-select" id="' .$item->get_id() . '" name="pdc[' . $item->get_id() . ']">';
                $select .= sprintf('<option value="">%s</option>', __('Use default revenue item', 'fattura24'));
                foreach ($pdc_list as $pdc_id => $pdc_name) {
                    $selected = selected($current_pdc, $pdc_id, false);
                    $select .= "<option value='{$pdc_id}' {$selected}>{$pdc_name}</option>";
                }
                $select .= '</select>';
                return $select;
            case 'action':
                $current_pdc = get_post_meta($item->get_id(), 'pdc', true);
                $button_class = $current_pdc ? 'fatt-24-hidden' : '';
                $button = '<button type="button" class="button action fatt24 ' . $button_class . '" data-product-id="' . $item->get_id() . '">' . __('Apply', 'fattura24') . '</button>';
                return $button;
            default:
                return print_r($item, true);
        }
    }

    public function column_cb($item) {
        return sprintf(
            '<input type="checkbox" name="product[]" value="%s" />', $item->get_id()
        );
    }
}

// azione ajax di salvataggio dell'opzione scelta nei metadati del prodotto
add_action('wp_ajax_save_pdc', __NAMESPACE__ . '\fatt_24_save_product_pdc');

function fatt_24_save_product_pdc() {
    if (!current_user_can('manage_options')) {
        wp_send_json_error('Permission denied');
        return;
    }

    if (!wp_verify_nonce(sanitize_text_field($_POST['nonce']), 'pdc_nonce')){
        wp_send_json_error('Invalid nonce');
        return;
    }

    $value = isset($_POST['value'])? (int) sanitize_text_field($_POST['value']) : '';
    $section = isset($_POST['section']) ? (string) sanitize_text_field($_POST['section']) : '';
    $ids = isset($_POST['ids']) ? sanitize_text_field($_POST['ids']) : '';
    $action = '';
    $success = false;

    switch ($section) {
        case 'default':
            $ids = isset($_POST['ids']) ? (string) sanitize_text_field($_POST['ids']) : '';
            $action = 'update_option';
            break;
        case 'table':
            $ids = (int) $ids;
            $action = 'update_meta';
        case 'bulk-action':
            // get an array of ids starting from a comma separated list of ids
            $ids = array_map(function ($value) { return (int) trim($value); }, explode(',', $ids));
            $action = 'bulk_update_meta';
            break;
        default:
            break;
    }

    switch ($action) {
        case 'update_option':
            $option_name = 'fatt-24-inv-pdc';
            $ids === 'products' ? update_option($option_name, $value) : update_option("$option_name-$ids", $value);
            $success = true;
            break;
        case 'update_meta':
            update_post_meta($ids, 'pdc', $value);
            $success = true;
            break;
        case 'bulk_update_meta':
            foreach($ids as $id) {
                update_post_meta($id, 'pdc', $value); 
            }
            $success = true;
            break;
        default:
            break;
    }

    if (true === $success) {
        wp_send_json_success(['message' => __('Item updated successfully', 'fattura24')]);
    } else {
        wp_send_json_error(['message' =>__('Invalid product ID or item', 'fattura24')]);
    } 
}

function fatt_24_show_pdc_options() {
    $options = get_option('pdc_list');
    $selected_pdc = [
     'products' => get_option('fatt-24-inv-pdc'),
     'shipping' => get_option('fatt-24-inv-pdc-shipping'),
     'fees' => get_option('fatt-24-inv-pdc-fees')
   ];

   $html = fatt_24_generate_selects($options, $selected_pdc);
   echo $html;
}

function fatt_24_generate_selects($options, $selected_pdc) {
    $output = '';
    $select_template = '<div class="fatt-24-select-container"><label for="%s">%s:</label><select class="fatt-24-custom-select"name="%s" id="%s">%s</select><button id="%s" class="button action fatt24 default %s">%s</button></div>';
    $option_template = '<option value="%s"%s>%s</option>';
   
    foreach($selected_pdc as $key => $value) {
        
        $options_html = '';
        $is_selected = false;
        foreach ($options as $option_value => $option_label) {
            $selected = ($option_value == $value) ? 'selected' : '';
            if ($selected) {
                $is_selected = true;
            }
            $options_html .= sprintf($option_template, $option_value, $selected, $option_label);
        }
        $button_class = $is_selected ? 'fatt-24-hidden' : '';

        $output .= sprintf(
            $select_template,
            $key,
            __(ucfirst($key), 'fattura24'),
            $key,
            $key,
            $options_html,
            $key,
           $button_class,
            __('Apply', 'fattura24')
        );
    }
    return $output;
}