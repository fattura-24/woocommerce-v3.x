<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * Metodi associati alla gestione delle tasse e dei codici natura
 * la parte visiva è in src/tax.php
 */
namespace Fattura24;

defined('ABSPATH') || die;

if (!class_exists('WP_List_Table')) {
    require_once(ABSPATH . 'wp-admin/includes/class-wp-list-table.php');
}

class Fattura24_Taxes_Table extends \WP_List_Table {
    private $items_data;

    public function __construct() {
        parent::__construct([
            'singular' => 'fattura_tax',
            'plural'   => 'fattura_taxes',
            'ajax'     => false
        ]);

        $this->fetch_data();
    }

    public function count_shipping_rates()
    {
        global $wpdb;

        $prefix = is_multisite()? $wpdb->base_prefix : $wpdb->prefix;
        $blog_id = is_multisite()? get_current_blog_id() : 1;
        $table_name = $prefix . 'fattura_tax'; 
        $shipping_rates_count = $wpdb->get_var(
            $wpdb->prepare(
                "SELECT COUNT(*) 
                FROM {$table_name}
                WHERE used_for_shipping = 1
                AND blog_id={$blog_id}",
            )
        );
        return (int) $shipping_rates_count;
    }

    private function fetch_data()
    {
        global $wpdb;
        $prefix = $wpdb->prefix;
        $base_prefix = is_multisite() ? $wpdb->base_prefix : $wpdb->prefix;
        $blog_id = is_multisite()? get_current_blog_id() : 1;

        $query = "SELECT wtr.*, ft.* 
                FROM {$prefix}woocommerce_tax_rates wtr
                LEFT JOIN {$base_prefix}fattura_tax ft ON wtr.tax_rate_id = ft.tax_id";

        if (is_multisite()) {
            $query .= " AND ft.blog_id = %d";
        }        

        $query .= " WHERE wtr.tax_rate = 0.0";

        $this->items_data = $wpdb->get_results(
            is_multisite() ?
            $wpdb->prepare($query, $blog_id) : $query,               
            ARRAY_A // voglio un array
        );
    }

    public function prepare_items() {
        global $wpdb;
        $this->_column_headers = [$this->get_columns(), [], $this->get_sortable_columns()];
        
        $per_page = 20;
        $current_page = $this->get_pagenum();
        $total_items = count($this->items_data);

        $this->set_pagination_args([
            'total_items' => $total_items,
            'per_page' => $per_page
        ]);

        $this->items = array_slice(
            $this->items_data, ($current_page - 1) * $per_page, $per_page);
    }

    public function get_columns() {
        return [
            'id'                       => __('Record ID', 'fattura24'),
            'tax_id'                   => __('Rate ID', 'fattura24'),
            'name'                     => __('Name', 'fattura24'),
            'natura'                   => __('Natura', 'fattura24'),
            'used_for_shipping'        => __('Used for shipping', 'fattura24'),
            'actions'                  => ''
        ];
    }

    public function column_default($item, $column_name) {
        switch ($column_name) {
            case 'id':
                return sprintf('<span class="fattura24-record-id" data-id=%s data-tax-id="%s">%s</span>', 
                $item['id'], $item['tax_id'], $item['id']);
            case 'tax_id':
                return $item['tax_rate_id'];    
            case 'name':
                return $item['tax_rate_name'];
            case 'natura':
                return $this->get_natura_options($item['tax_code'], $item['id'], $item['tax_rate_id']);
            case 'used_for_shipping':
                $checked = $item['used_for_shipping'] === '1' ? 'checked' : '';
                return sprintf(
                    '<input type="checkbox" class="fattura24-cb" data-id="%s" data-used-for-shipping-id="%d" %s />', 
                    $item['id'],
                    $item['tax_rate_id'], 
                    $checked,
                );
            case 'actions':
                $style = $item['id'] && $item['tax_code'] ? 'style="display:none;"' : '';
                return sprintf(
                    '<button class="button apply-natura" data-id="%d" data-row-id="%d" %s>%s</button>',
                    $item['id'],
                    $item['tax_rate_id'],
                    $style,
                    __('Apply', 'fattura24')
                );
            default:
                return print_r($item, true);
        }
    }

   
    private function get_natura_options($selected = '', $tax_id = '', $row_id = '') {
        $options = [
            'N1' => [
                'label' => 'N1',
                'options' => [
                    'N1' => __('N1 - escluse ex art.15', 'fattura24')
                ]
            ],
            'N2' => [
                'label' => 'N2',
                'options' => [
                    'N2.1' => __('N2.1 - non soggette ad IVA ai sensi degli artt. da 7 a 7-septies del DPR 633/72', 'fattura24'),
                    'N2.2' => __('N2.2 - non soggette - altri casi', 'fattura24')
                ]
            ],
            'N3' => [
                'label' => 'N3',
                'options' => [
                    'N3.1' => __('N3.1 - non imponibili - esportazioni', 'fattura24'),
                    'N3.2' => __('N3.2 - non imponibili - cessioni intracomunitarie', 'fattura24'),
                    'N3.3' => __('N3.3 - non imponibili - cessioni verso San Marino', 'fattura24'),
                    'N3.4' => __('N3.4 - non imponibili - operazioni assimilate alle cessioni all\'esportazione', 'fattura24'),
                    'N3.5' => __('N3.5 - non imponibili - a seguito di dichiarazioni d\'intento', 'fattura24'),
                    'N3.6' => __('N3.6 - non imponibili - altre operazioni che non concorrono alla formazione del plafond', 'fattura24')
                ]
            ],
            'N4' => [
                'label' => 'N4',
                'options' => [
                    'N4' => __('N4 - esenti', 'fattura24')
                ]
            ],
            'N5' => [
                'label' => 'N5',
                'options' => [
                    'N5' => __('N5 - regime del margine / Iva non esposta in fattura', 'fattura24')
                ]
            ],
            'N6' => [
                'label' => 'N6',
                'options' => [
                    'N6.1' => __('N6.1 - inversione contabile - cessione di rottami e altri materiali di recupero', 'fattura24'),
                    'N6.2' => __('N6.2 - inversione contabile - cessione di oro e argento puro', 'fattura24'),
                    'N6.3' => __('N6.3 - inversione contabile - subappalto nel settore edile', 'fattura24'),
                    'N6.4' => __('N6.4 - inversione contabile - cessione di fabbricati', 'fattura24'),
                    'N6.5' => __('N6.5 - inversione contabile - cessione di telefoni cellulari', 'fattura24'),
                    'N6.6' => __('N6.6 - inversione contabile - cessione di prodotti elettronici', 'fattura24'),
                    'N6.7' => __('N6.7 - inversione contabile - prestazioni comparto edile e settori connessi', 'fattura24'),
                    'N6.8' => __('N6.8 - inversione contabile - operazioni settore energetico', 'fattura24'),
                    'N6.9' => __('N6.9 - inversione contabile - altri casi', 'fattura24')
                ]
            ],
            'N7' => [
                'label' => 'N7',
                'options' => [
                    'N7' => __('N7 - IVA assolta in altro stato UE', 'fattura24')
                ]
            ]
        ];
        
        $html = "<select name='tax_code' id='tax-code-" . $row_id . "' class='tax-codes' data-id='" . $tax_id . "' data-row-id='" . $row_id . "'>";
        $html .= "<option value=''>" . __('Choose an item...', 'fattura24') . "</option>";

        foreach ($options as $group) {
            $html .= "<optgroup label='" . esc_attr($group['label']) . "'>";
            foreach ($group['options'] as $value => $label) {
                $html .= sprintf(
                    "<option value='%s'%s>%s</option>",
                    esc_attr($value),
                    selected($selected, $value, false),
                    esc_html($label)
                );
            }
            $html .= "</optgroup>";
        }

        $html .= "</select>";

        return $html;
    }
}

add_action('wp_ajax_update_tax_code', __NAMESPACE__ .'\fatt_24_update_tax');


function fatt_24_get_natura_records($tax_id = null) {
    global $wpdb;
    $prefix = is_multisite() ? $wpdb->base_prefix : $wpdb->prefix;
    $blog_id = is_multisite() ? get_current_blog_id() : 1;
    $table_name = $prefix . 'fattura_tax';
    
    $query = "SELECT tax_id, tax_code, used_for_shipping FROM $table_name WHERE blog_id = %d";
    $params = [$blog_id];


    if (isset($tax_id)) {
        $query .= " AND tax_id = %d";
        $params[] = $tax_id;
    }

    $sql = $wpdb->prepare($query, $params);
    $result = $wpdb->get_results($sql);
    return $result;
}

function fatt_24_get_zero_shipping_tax_natura()
{
    $resultArray = [];
    $natura_records = fatt_24_get_natura_records();
    
    if (!empty($natura_records)) {
        $key = array_search(1, 
            array_column($natura_records, 'used_for_shipping'));
        $resultArray = $key ? $natura_records[$key] : $natura_records[0];
    }

    return $resultArray;
}

function fatt_24_update_tax() {
   
    if (!current_user_can('manage_options')) {
        wp_send_json_error('Permission denied');
        return;
    }

    if (!wp_verify_nonce(sanitize_text_field($_POST['nonce']), 'taxes_nonce')){
        wp_send_json_error('Invalid nonce');
        return;
    }

    $tax_id = isset($_POST['tax_id']) ? (int) $_POST['tax_id'] : 0; 
    $row_id = isset($_POST['row_id']) ? (int) $_POST['row_id']: 0;
    $tax_code = isset($_POST['tax_code']) ? sanitize_text_field($_POST['tax_code']) : ''; 
    $blog_id = is_multisite() ? (isset($_POST['blog_id']) ? (int) $_POST['blog_id'] : get_current_blog_id()) : 1;
    
    /**
     * Istanzio la classe cosicché solo una aliquota potrà essere usata per la spedizione
     */
    $tax_table = new Fattura24_Taxes_Table();
    $shipping_rates_count = $tax_table->count_shipping_rates();
    $used_for_shipping = ($shipping_rates_count > 1) ? 0 : (isset($_POST['used_for_shipping']) ? (int) $_POST['used_for_shipping'] : 0);

    $data = [
        'tax_id' => (int) $tax_id,
        'row_id' => (int) $row_id,
        'tax_code' => (string) $tax_code,
        'used_for_shipping' => (int) $used_for_shipping,
        'blog_id' => (int) $blog_id
    ];

    $action = fatt_24_save_natura_record($data);

    if (in_array($action, ['insert', 'update'])) {
        wp_send_json_success(__('Record stored successfully', 'fattura24'));
    } else {
        wp_send_json_error(__('Record not saved', 'fattura24'));
    }  

}

function fatt_24_save_natura_record($data) {
    global $wpdb;

    $prefix = is_multisite() ? $wpdb->base_prefix : $wpdb->prefix;
    $table_name = $prefix . "fattura_tax";
    $result = '';
    
    $blog_id = isset($data['blog_id']) ? $data['blog_id'] : (is_multisite() ? get_current_blog_id() : 1);

     // Se 'used_for_shipping' è 1, resetta tutti gli altri record a 0 per questa colonna
     if ((int) $data['used_for_shipping'] === 1) {
        $wpdb->update(
            $table_name,
            ['used_for_shipping' => 0],
            ['blog_id' => $blog_id],
            ['%d'],
            ['%d']
        );
    }

    $existing_record = $wpdb->get_row(
        $wpdb->prepare(
        "SELECT id FROM $table_name WHERE tax_id = %d AND blog_id = %d",
        $data['row_id'],
        $blog_id
    ));
    
    if (!$existing_record) {
        $wpdb->insert(
                $table_name,
                [
                    'tax_id' => $data['row_id'],
                    'tax_code' => $data['tax_code'],
                    'used_for_shipping' => $data['used_for_shipping'],
                    'blog_id' => $blog_id
                ],
                ['%d', '%s', '%d', '%d'] 
            );
        $result = 'insert';
    } else {
        $wpdb->update(
            $table_name, //table
            [
                'tax_code' => $data['tax_code'], 
                'used_for_shipping' => $data['used_for_shipping'],
            ],
            ['id' => $existing_record->id], //where
            ['%s', '%d'],
            ['%d'] //where format
        );
        $result = 'update';
    }

    return $result;
}