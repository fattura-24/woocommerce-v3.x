<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * Descrizione: gestisce la tab "Tipologie di pagamento" della schermata di impostazioni
 * (parte visiva)
 * 
 * Le funzioni di servizio sono in methods/met_payment_types.php
 */
namespace Fattura24;

defined('ABSPATH') || die;

$filesToInclude = [ 
    'methods/met_payment_types.php'
];

foreach ($filesToInclude as $file) {
    require_once FATT_24_CODE_ROOT . $file;
}


function fatt_24_show_payment_types() {
    $payment_table = new Fattura24_Payment_Types();
    $payment_table->prepare_items();

    ?>
    <div class='wrap'>
        <h2></h2>
        <?php 
            fatt_24_get_link_and_logo(__('', 'fatt-24-payment-types')); 
            echo fatt_24_build_nav_bar();
        ?>
        <div style="display: flex; flex-direction: row; justify-content: space-between; align-items: flex-start;">
            <div style="flex: 1;">
                <form method="post">
                    <div class="tablenav top">
                        <?php 
                            $payment_table->display(); 
                        ?>
                    </div> 
                </form>
            </div>
            <div style="width: 250px; margin-left: 20px;">
                <?php echo fatt_24_infobox(); ?>
            </div>
        </div> 
        <div style="margin-top: 20px;">
            <div style="font-size:120%; padding-bottom: 10px;">
                <?php 
                    _e('This section is useful when you are issuing electronic invoices to link the correct payment type code to each payment method.', 'fattura24');
                    echo '<br />';
                    _e('Some methods have got its defined and fixed code, in other cases you should choose from the list the appropriate item.', 'fattura24');
                    echo '<br />';
                    _e('In lack of your choice the code MP08 will be used as default type, since in an e-commerce probably credit card is the most used payment method.', 'fattura24');
                ?>
            </div>
        </div>     
    </div>
    <?php
}