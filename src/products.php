<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * Descrizione: gestisce la tab "Voci di ricavo" della schermata di impostazioni
 * (parte visiva)
 * 
 * Le funzioni di servizio sono in methods/met_products.php
 */
namespace Fattura24;

defined('ABSPATH') || die;

$filesToInclude = [ 
    'methods/met_products.php'
];

foreach ($filesToInclude as $file) {
    require_once FATT_24_CODE_ROOT . $file;
}

function fatt_24_show_products() {
    $table = new Fattura24_PDC_List_Table();
    $table->prepare_items();
    ?>
        <div class='wrap fatt-24-products'>
        <h2></h2>
        <?php 
            fatt_24_get_link_and_logo(__('', 'fatt-24-products')); 
            echo fatt_24_build_nav_bar();
        ?>
        <div class="fatt-24-main-container">
            <div class="fatt-24-child-container">
                <div class="fatt-24-default-values-wrapper">
                    <?php echo '<h3>' . __('Default values', 'fattura24') . '</h3>' . '<br / >'; 
                            fatt_24_show_pdc_options(); 
                    ?>
                </div>
                <form method="post">
                    <div class="fatt-24-tablenav top">
                        <div class="fatt-24-h3-wrapper">
                            <?php 
                                echo '<h3>' . __('Per product', 'fattura24') . '</h3>' . '<br / >'; 
                                $table->display(); 
                            ?>
                        </div>
                    </div> 
                </form>
            </div>
            <div class="fatt-24-infobox">
                <?php echo fatt_24_infobox(); ?>
            </div>
        </div>
        <div class="fatt-24-instructions-wrapper">
            <div class="fatt-24-instructions-content">
                <?php 
                    _e('This section helps you to manage chart of accounts in your Fattura24 account.', 'fattura24');
                    echo '<br />';
                    _e('For each product you may link the appropriate revenue item, and you may also link the same item for a group of products by multiple selection.', 'fattura24');
                    echo '<br />';
                    _e('In lack of your choice Fattura24 will use the default revenue item you chose for products. See data above the table for more.', 'fattura24');
                ?>
            </div>
        </div>    
    </div>
    <?php
}