<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * File di gestione delle impostazioni del plugin (Impostazioni generali)
 * parte visiva
 * 
 * le funzioni di servizio sono in settings_uty.php, methods/met_settings.pages.php, methods/met_settings.sections.php
 */

namespace Fattura24;

if (!defined('ABSPATH')) {
    exit;
}

$filesToInclude = [
    'settings_uty.php',
    'api/api_get_templates.php',
    'api/api_get_pdc.php',
    'api/api_get_numerators.php',
    'api/api_wrapper.php',
    'methods/met_get_templates.php',
    'methods/met_get_pdc.php',
    'methods/met_get_numerators.php',
    'methods/met_settings_pages.php',
    'methods/met_settings_sections.php',
];

foreach ($filesToInclude as $file) {
    require_once FATT_24_CODE_ROOT . $file;
}


if (is_admin()) {
    /**
    * Modifica del 26/08/2020:
    * ora il metodo ritorna l'intero array dei dati del plugin
    * per ottenere la versione => $plugin_data['Version'].
    * Modifica inserita per aggiungere il nome del plugin nei messaggi di errore
    * Davide Iandoli
    */

    // versione di woocommerce
    function fatt_24_woocommerce_version_check()
    {
        if (fatt_24_isWooCommerceInstalled()) {
            global $woocommerce;
            if (version_compare($woocommerce->version, '3.0.0', ">=")) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }

    // restituisce il messaggio a valle del controllo sulla versione del plugin
    function fatt_24_getVersionCheckMessage()
    {
        $pluginDate = '2020/12/09 16:00';
        $versionCheckMessage = '';
        $checkWooVersion = fatt_24_woocommerce_version_check(); // controllo la versione di woocommerce
        if (!$checkWooVersion) {
            $message = __('Warning: this plugin is not suitable for current WooCommerce version. Click ', 'fattura24');
            $message .= fatt_24_a(array('href' => 'https://www.fattura24.com/woocommerce-plugin-fatturazione', 'target' => '_blank'), __('here ', 'fattura24'));
            $message .= __('to download the correct version of the plugin', 'fattura24');
            $versionCheckMessage .= fatt_24_getMessageHtml($message, 'error', true);
        }
        return $versionCheckMessage;
    }

    // ottiene le informazioni sull'ambiente per includerle nel tracciato di log
    function fatt_24_getInfo()
    {
        global $woocommerce, $wpdb, $wp_version;
        
        return array(
            'PHP version' => PHP_VERSION,
            'MySQL version' => $wpdb->db_version(),
            'WordPress version' => $wp_version,
            'Multisite' => is_multisite() ? 'yes' : 'no',
            'Rtl' => is_rtl() ? 'yes' : 'no',
            'WP_MEMORY_LIMIT' => WP_MEMORY_LIMIT,
            'WP_MAX_MEMORY_LIMIT' => WP_MAX_MEMORY_LIMIT,
            'WP_DEBUG' => WP_DEBUG == 1 ? 'enabled' : 'disabled',
            'WP_DEBUG_LOG' => WP_DEBUG_LOG == 1 ? 'enabled' : 'disabled',
            'PHP memory_limit' => ini_get('memory_limit'),
            'WooCommerce Version' => $woocommerce->version,
            'WooCommerce Checkout Blocks' => fatt_24_is_checkout_block()? 'yes' : 'no',
            'WooCommerce Fattura24 Version' => FATT_24_PLUGIN_DATA['Version'],
            'Shop address' =>  get_permalink(wc_get_page_id('shop')),
            'Active theme' => wp_get_theme(),
            'Active plugins' => fatt_24_get_plugin_info()
        );
    }
}


// crea le sezioni della schermata di impostazioni principale e le gestisce
function fatt_24_init_settings()
{
    static $cached_logs = [];
    
    $fe_issue_updated = fatt_24_get_installation_log('updated fe issue number default option', $cached_logs);
    if (!$fe_issue_updated) {
        fatt_24_default_sezionale_fe();
        fatt_24_insert_installation_log('updated fe issue number default option');
        $cached_logs['updated fe issue number default option'] = true;
    }

    $billing_cb_updated = fatt_24_get_installation_log('updated billing cb postmeta');
    if (!$billing_cb_updated) {
        fatt_24_update_actions();
    }

    // Singole sotto-sezioni della pagina "Impostazioni Generali"
    $sect_key = fatt_24_info();
    $sect_addrbook = fatt_24_addrbook();
    $sect_orders = fatt_24_orders();
    $sect_invoices = fatt_24_invoices();
    $sect_optional_settings = fatt_24_optional_settings();
    $sect_logs = fatt_24_advanced();

    // NB: i primi 2 parametri passati qui sotto sono gli <ID> della pagina Impostazioni Generali e del Gruppo contenuto nella pagina
    // CTRL: v. in futuro di cambiare i nomi delle costanti aggiungendo il suffisso "_ID")
    // Questa funzione rende utilizzabili i 2 metodi wordpress "settings_fields()" e "do_settings_sections()" (v. più sotto nel render).
    fatt_24_setup_settings_page(FATT_24_SETTINGS_PAGE, FATT_24_SETTINGS_GROUP, array($sect_key, $sect_addrbook, $sect_orders, $sect_invoices, $sect_optional_settings, $sect_logs));
}


// visualizza una specifica pagina di impostazioni
function fatt_24_show_settings()
{
   
    /**
    * Qui eseguo le chiamate API solo se ho la chiave nel campo di input
    * n.b.: il campo sarà sempre vuoto per le nuove installazioni
    * fix del 18.08.2020 Davide Iandoli
    */
    $templates_result = [];

    if (!empty(get_option(FATT_24_OPT_API_KEY))) {
       $templates_result = fatt_24_getTemplate(true);
       fatt_24_getSezionale($templates_result['api_status']);
       fatt_24_getPdc($templates_result['api_status']);
    }

    fatt_24_init_settings(); ?>
  
	<div class='wrap'>
    <!-- https://wordpress.stackexchange.com/questions/220650/how-to-change-the-location-of-admin-notice-in-html-without-using-javascript/220735 -->
    <h2></h2>
   
    <?php fatt_24_get_link_and_logo(__('', 'fattura24'));
    echo fatt_24_build_nav_bar(); ?>

   	<div>
        <table width="100%">
            <tr>
                <td>   
                    <div>
	                    <form method='post' action='options.php'>
     
	                    <?php

                            // Questa è la pagina "IMPOSTAZIONE GEN"RALI" del Modulo di Fattura24
                            // Le varie sotto-sezioni sono Info, Dati Cliente, Ordini, Fatture, Configurazioni opzionali, Avanzate

                            // Le sotto-sezioni sono create dalla funzione fatt_24_setup_settings_page (v. sopra) che sta 
                            // nel file settings_uty.php

                    
                            // Se la chiamata API fallisce nascondo il pulsante di salvataggio impostazioni
                            $style = isset($templates_result['api_status']) && false === $templates_result['api_status'] ? array('style' => 'display: none;'): [];

                            // Bottone "Salva Impostazioni" di "sopra"
                            submit_button(__('Save Settings!', 'fattura24'), 'primary', 'submit_up', true, $style);

                            // Funzione di wordpress che registra/inizializza le varie sotto-sezioni HTML della sezione "Impostazioni Generali"                            
                            // V. documentazione wordpress.org su funzione "settings_fields"
                            settings_fields(FATT_24_SETTINGS_GROUP);

                            // Funzione di wordpress che inserisce/stampa l'intera sezione "Impostazioni Generali" con dentro le varie sotto-sezioni
                            // V. documentazione wordpress.org su funzione "settings_fields"
                            do_settings_sections(FATT_24_SETTINGS_PAGE);

                           // Bottone "Salva Impostazioni" "di sotto"
                            submit_button(__('Save Settings!', 'fattura24'), 'primary', 'submit_down', true, $style); // different id for submit buttons 
                        ?>
                        </form>
                    </div>
                </td>
                <td style="width:250px; vertical-align: top;">

  	                <?php
                           echo fatt_24_infobox('fatt-24-settings'); ?>
                </td>
            </tr>
        </table>
    </div>
	</div>
<?php
}
