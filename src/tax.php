<?php
/**
 * Questo file è parte del plugin WooCommerce v3.x di Fattura24
 * Autore: Fattura24.com <info@fattura24.com>
 *
 * Descrizione: gestisce la tab "Configurazione Tassa" della schermata di impostazioni
 * (parte visiva)
 * 
 * Le funzioni di servizio sono in methods/met_tax.php
 */
namespace Fattura24;

defined('ABSPATH') || die;

$filesToInclude = [ 
    'methods/met_tax.php'
];

foreach ($filesToInclude as $file) {
    require_once FATT_24_CODE_ROOT . $file;
}

function fatt_24_show_tax() {
    $table = new Fattura24_Taxes_Table();
    $table->prepare_items();
   
    ?>
    <div class='wrap'>
        <h2></h2>
        <?php 
            fatt_24_get_link_and_logo(__('', 'fatt-24-tax')); 
            echo fatt_24_build_nav_bar();
        ?>
         <div style="display: flex; flex-direction: row; justify-content: space-between; align-items: flex-start;">
            <div style="flex: 1;">
                <form method="post">
                    <div class="tablenav top">
                        <?php 
                            $table->display(); 
                        ?>
                    </div> 
                </form>
            </div>
            <div style="width: 250px; margin-left: 20px;">
                <?php echo fatt_24_infobox(); ?>
            </div>
        </div>    
        <div style="margin-top: 20px;">
            <div style="font-size:120%; padding-bottom: 10px;">
                <?php 
                    _e('This section is useful when you are issuing electronic invoices with products or services using zero rate VAT.', 'fattura24');
                    echo '<br />';
                    _e('According to AdE specs, an electronic invoice in which products or services are using a zero rate VAT and lacking natura code is subject to rejection by SdI.', 'fattura24');
                    echo '<br />';
                    _e('Please follow the instructions below to link a natura code to all your zero VAT rates and issue correct electronic invoices.', 'fattura24');
                ?>
            </div>
            <div style="font-size:150%; font-weight:bold;">
                <?php _e('User instructions', 'fattura24') ?>
            </div>
            <ol style="padding-left: 20px;">
                <li style="font-size:120%;">
                    <?php _e('Configure in Woocommerce->Settings->Tax one ore more zero rated taxes', 'fattura24')?>
                </li>
                <li style="font-size:120%;">
                    <?php _e('Warning: tax name should match the legal reference / natura.', 'fattura24')?>
                </li>
                <li style="font-size:120%;">
                    <?php _e('Save the changes: the table will display all zero rates and all natura options available.', 'fattura24')?>
                </li>
                <li style="font-size:120%;">
                    <?php _e('Choose natura for each row, choose if you want to use it for shipping and click on apply button to save the changes.', 'fattura24')?>
                </li>
            </ol>
        </div>
    </div>
    <?php
}
